interface Object
{
    [idx: string]: any;
}

module ak
{
    if (!<File>window['File'] || !<FileReader>window['FileReader']
        || !<FileList>window['FileList'] || !<Blob>window['Blob'])
    {
        alert('Not supported');
        return;
    }

    window.addEventListener('load', function ()
    {
        var dropTarget = document.createElement('div');
        dropTarget.textContent = 'Drag and drop here';
        dropTarget.className = 'drop-target';

        dropTarget.addEventListener('dragover', function (evt)
        {
            evt.stopPropagation();
            evt.preventDefault();
            evt.dataTransfer.dropEffect = 'copy';
            dropTarget.textContent = 'Drop...';
        });
        dropTarget.addEventListener('drop', function (evt)
        {
            evt.stopPropagation();
            evt.preventDefault();
            dropTarget.textContent = 'Drag and drop here';

            var items = (<File[]>Array.prototype.slice.call(evt.dataTransfer.files, 0))
                .filter(x => x.type.lastIndexOf('image', 0) === 0);
            if (items.length !== 1)
            {
                alert('You must select 1 image file.');
                return;
            }

            var reader = new FileReader();
            reader.onload = readImageFile;
            reader.readAsDataURL(items[0]);
        });

        document.body.appendChild(dropTarget);
    });

    var levelDrawers: LevelDrawer[] = [];

    function readImageFile(e: any)
    {
        var dataUrl: string = (<FileReader>e.target).result;
        var img = document.createElement('img');
        img.src = dataUrl;
        levelDrawers.push(new LevelDrawer(img));
    }

    interface IPoint
    {
        x: number;
        y: number;
    }

    class LevelDrawer
    {
        private drawer = document.createElement('div');

        private canvas = document.createElement('canvas');
        private ctx = this.canvas.getContext('2d');

        // Input
        private eScaleX = document.createElement('input');
        private eScaleY = document.createElement('input');
        private eSnap = document.createElement('input');

        // Output
        private output = document.createElement('textarea');
        private mouseAt = document.createElement('span');

        private scaleX: number = 1;
        private scaleY: number = 1;
        private snap: number = 1;

        private points: IPoint[] = [];
        private renderImage = true;

        constructor(private img: HTMLImageElement)
        {
            // Set properties
            this.drawer.className = 'drawer';

            this.canvas.width = img.width;
            this.canvas.height = img.height;

            this.output.style.display = 'none';
            this.output.style.width =  '500px';
            this.output.style.height = '300px';

            // Add to DOM
            document.body.appendChild(this.drawer);
            this.drawer.appendChild(this.canvas);

            // Controls
            var container = LevelDrawer.createButtons(this.drawer,
                [
                    {
                        text: 'Delete',
                        handler: this.Remove
                    },
                    {
                        text: 'Render image',
                        handler: this.toggleRenderImage
                    },
                    {
                        text: 'Remove last',
                        handler: this.removeLast
                    },
                    {
                        text: 'Remove all',
                        handler: this.removeAll
                    },
                    {
                        text: 'Import point',
                        handler: this.importPoint
                    },
                    {
                        text: 'Generate',
                        handler: this.generate
                    }
                ]);

            container.appendChild(this.mouseAt);
            [this.eScaleX, this.eScaleY].forEach((elem) =>
            {
                elem.addEventListener('change', this.scaleChanged);
                elem.type = 'number';
                elem.value = '50';
                container.appendChild(elem);
            });

            this.eSnap.addEventListener('change', this.snapChanged);
            this.eSnap.type = 'number';
            this.eSnap.value = '1';
            container.appendChild(this.eSnap);

            // Output
            this.drawer.appendChild(this.output);

            // Add event listeners
            this.canvas.addEventListener('click', this.mouseClick);
            this.canvas.addEventListener('mousemove', this.mouseMove);

            // Draw canvas
            this.scaleChanged();
            this.update();
        }

        private update = () =>
        {
            var i: number;

            this.ctx.save();
            this.ctx.setTransform(1, 0, 0, 1, 0, 0);
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.restore();

            if (this.renderImage)
                this.ctx.drawImage(this.img, 0, 0);

            // Draw points
            this.ctx.strokeStyle = 'rgba(64, 255, 64, 128)';
            for (i = 0; i < this.points.length; i++)
            {
                this.ctx.beginPath();
                this.ctx.arc(
                    Math.floor(this.points[i].x / this.snap) * this.snap,
                    Math.floor(this.points[i].y / this.snap) * this.snap,
                    4, 0, Math.PI * 2);
                this.ctx.closePath();
                this.ctx.stroke();
            }

            // Draw connecting lines
            if (this.points.length > 1)
            {
                this.ctx.strokeStyle = 'rgba(64, 64, 255, 128)';
                this.ctx.beginPath();
                this.ctx.moveTo(
                    Math.floor(this.points[0].x / this.snap) * this.snap,
                    Math.floor(this.points[0].y / this.snap) * this.snap);
                for (i = 1; i < this.points.length; i++)
                {
                    this.ctx.lineTo(
                        Math.floor(this.points[i].x / this.snap) * this.snap,
                        Math.floor(this.points[i].y / this.snap) * this.snap);
                }
                this.ctx.stroke();
            }
        };

        private scaleChanged = () =>
        {
            var sX = parseFloat(this.eScaleX.value);
            var sY = parseFloat(this.eScaleY.value);
            if (!isNaN(sX)) this.scaleX = sX;
            if (!isNaN(sY)) this.scaleY = sY;
        };

        private snapChanged = () =>
        {
            var newSnap = parseFloat(this.eSnap.value);
            if (!isNaN(newSnap) && newSnap != this.snap)
            {
                this.snap = newSnap;
                this.update();
            }
        };

        private mouseClick = (e: MouseEvent) =>
        {
            e.stopPropagation();
            e.preventDefault();

            var x = e.offsetX, y = e.offsetY;

            if (e.shiftKey && this.points.length !== 0)
            {
                var lastPoint = this.points[this.points.length - 1];
                var diffX = lastPoint.x - x;
                var diffY = lastPoint.y - y;
                var angle = Math.atan2(diffY, diffX);
                angle = Math.round(angle * 4 / Math.PI) * Math.PI / 4;
                var dist = Math.sqrt(diffX * diffX + diffY * diffY);
                x = lastPoint.x - Math.cos(angle) * dist;
                y = lastPoint.y - Math.sin(angle) * dist;
            }

            this.points.push({ x: x, y: y });
            this.update();
        };

        private mouseMove = (e: MouseEvent) =>
        {
            this.mouseAt.textContent =
            "X: " + (Math.floor(e.offsetX / this.snap) * this.snap / this.scaleX).toFixed(3) +
            "   Y: " + ((this.img.height - Math.ceil(e.offsetY / this.snap) * this.snap) / this.scaleY).toFixed(3);
        };

        private static createButtons(parent: Node, buttons: { text: string; handler: (evt?: MouseEvent) => any; }[])
        {
            var container = document.createElement('div');
            container.className = 'drawer-controls';

            for (var i = 0; i < buttons.length; i++)
            {
                var btn = document.createElement('button');
                btn.textContent = buttons[i].text;
                btn.addEventListener('click', buttons[i].handler);
                container.appendChild(btn);
            }

            parent.appendChild(container);
            return container;
        }

        private toggleRenderImage = () =>
        {
            this.renderImage = !this.renderImage;
            this.update();
        };

        private removeLast = () =>
        {
            if (this.points.length === 0) return;
            this.points.pop();
            this.update();
        };

        private removeAll = () =>
        {
            if (this.points.length === 0) return;
            this.points = [];
            this.update();
        };

        private importPoint = () =>
        {
            var x = parseFloat(prompt('X: '));
            var y = parseFloat(prompt('Y: '));
            if (isNaN(x) || isNaN(y)
                || x === Infinity || x === -Infinity
                || y === Infinity || y === -Infinity)
            {
                alert('Enter correct floats.');
                return;
            }

            // Convert to pixels
            x *= this.scaleX;
            y *= this.scaleY;
            // Make origin top-left instead of bottom-left
            y = this.img.height - y;
            this.points.push({ x: x, y: y });
            this.update();
        };

        private generate = () =>
        {
            var output: string;
            if (this.points.length === 0)
            {
                output = 'No points...';
            }
            else
            {
                output = '';
                for (var i = 0; i < this.points.length; i++)
                {
                    var x = (Math.floor(this.points[i].x / this.snap) * this.snap / this.scaleX).toFixed(3);
                    while (x.charAt(x.length - 1) === '0') x = x.substr(0, x.length - 1);

                    var y = ((this.img.height - Math.ceil(this.points[i].y / this.snap) * this.snap) / this.scaleY).toFixed(3);
                    while (y.charAt(y.length - 1) === '0') y = y.substr(0, y.length - 1);

                    output += 'b2Vec2(' + x + 'f, ' + y + 'f)\r\n';
                }
                output = output.substr(0, output.length - 2);
            }
            this.output.textContent = output;
            this.output.style.display = 'block';
        };

        Remove = () =>
        {
            document.body.removeChild(this.drawer);
        };
    }
}