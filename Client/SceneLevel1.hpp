#pragma once
#include "SceneLevel.hpp"

namespace ak
{
	class SceneLevel1 :
		public SceneLevel
	{
	public:
		SceneLevel1(GameContext& context);
		virtual ~SceneLevel1();

		void Load() override;
		void HandleEvent(const sf::Event& event) override;
		void Update(const float deltaTime) override;
		void Unload() override;

		const sf::Vector2i GetSize() const override;
		void OnPlayerDied() override;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	};
}