#pragma once
#include "ColoredDoor.hpp"
#include "Box2dSprite.hpp"

namespace ak
{
	class SimpleDoor :
		public ColoredDoor
	{
	public:
		SimpleDoor(GameContext& context, sf::Color color);

		b2Body* CreateBody(b2World& world, ContactManager& contactManager) override;

		void Load() override;
		void Update(const float deltaTime) override;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override;

		void OnOpened() override;
		void OnClosed() override;
	private:
		uptr<const Box2dSprite> _frame;
		uptr<Box2dSprite> _slider;
	};
}