/*

The resource manager loads assets from the filesystem.
Because this is more a prototype kinda game than a real game engine,
the asset loading will be simple. Load assets upon request, unload
then upon destruction.

All loaded assets are owned by the ResourceManager, and there is no
need to track or dispose them anywhere else.

Owned by GameContext.

*/

#pragma once
#include <unordered_map>

namespace ak
{
	class ResourceManager {
	public:
		// Pretty self-explanatory, get the references.
		const sf::Font& GetFont(const std::string name);
		const sf::Texture& GetTexture(const std::string name);
		const sf::SoundBuffer& GetSound(const std::string name);
	private:
		// Resources are cached in memory in an unordered_map.
		// All resources must be const, so that multiple consumers can't
		// the resources that another piece of code is also using.
		template<typename TSfml>
		using ResCache = std::unordered_map<std::string, uptr<const TSfml>>;

		// One function to test the unordered_map for a cached version.
		// If there is none it'll load one and store a unique pointer to
		// it in the unordered_map.
		template<typename TSfml, typename ...Args>
		static const TSfml& loadItem(const std::string name, ResCache<TSfml>& cache, Args&& ...arguments);

		// Caches for specific types
		ResCache<sf::Font> _fontCache;
		ResCache<sf::Texture> _textureCache;
		ResCache<sf::SoundBuffer> _soundCache;
	};
}