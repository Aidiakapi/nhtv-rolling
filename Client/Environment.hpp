#pragma once
#include "Box2dObject.hpp"
#include <vector>

namespace ak
{
	class Environment :
		public Box2dObject
	{
	public:
		Environment(GameContext& context, const std::vector<b2Vec2> points, bool isLoop);
		virtual ~Environment();

		b2Body* CreateBody(b2World& world, ContactManager& contactManager) override;

		void Update(const float deltaTime) override;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override;

	private:
		uptr<sf::VertexArray> _vertArr;
		uptr<b2ChainShape> _chain;
	};
}