/*

The ObjectWorld will create, update and destroy a b2World.
It can be deactivated to stop updating the b2World.

*/

#pragma once
#include "GameObject.hpp"
#include "ContactManager.hpp"

namespace ak
{
	class GameContext;
	class ObjectWorld final :
		public GameObject
	{
	public:
		ObjectWorld(GameContext& context);

		inline b2World* GetWorld() { return _world.get(); }
		inline ContactManager* GetContactManager() { return _contactManager.get(); }

		void Load() override;
		void Update(const float deltaTime) override;
		inline void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override { }

	private:
		uptr<b2World> _world;
		uptr<ContactManager> _contactManager;
	};
}