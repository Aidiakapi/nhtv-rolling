#pragma once
#include "Box2dObject.hpp"
#include "Box2dSprite.hpp"

namespace ak
{
	
	class ColoredKey :
		public Box2dObject
	{
	public:
		void Load() override;
		void Unload() override;

		sf::Color GetColor() const;
	protected:
		ColoredKey(GameContext& context, sf::Color color, std::string name = "");
		void pickUp();

	private:
		const sf::Color _color;
		bool _pickedUp;
	};
}