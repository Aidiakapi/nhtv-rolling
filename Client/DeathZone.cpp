#include "stdafx.hpp"
#include "DeathZone.hpp"

#include "SceneLevel.hpp"
#include "GameContext.hpp"
#include "ContactManager.hpp"
#include "CollisionCategory.hpp"

namespace ak
{
	DeathZone::DeathZone(GameContext& context, const sf::Vector2f size) :
		Box2dObject(context),
		_size(size),
		_beginContactListener(0),
		_madeContact(false)
	{
		Visible = false;
	}

	b2Body* DeathZone::CreateBody(b2World& world, ContactManager& contactManager)
	{
		b2BodyDef bDef;
		bDef.type = b2_staticBody;
		bDef.position = b2Vec2(getPosition().x, getPosition().y);

		b2PolygonShape shp;
		// Can't use the SetAsBox method because we don't want the origin in the center.
		b2Vec2 box[] =
		{
			b2Vec2(0.f, 0.f),
			b2Vec2(0.f, _size.y),
			b2Vec2(_size.x, _size.y),
			b2Vec2(_size.x, 0.f)
		};
		shp.Set(box, 4);
		b2FixtureDef fDef;
		fDef.shape = &shp;
		fDef.filter.categoryBits = CollisionCategory::Entity;
		fDef.filter.maskBits = CollisionCategory::Player;
		fDef.isSensor = true;

		auto body = world.CreateBody(&bDef);
		auto fix = body->CreateFixture(&fDef);

		_beginContactListener = contactManager.RegisterBeginContact(fix,
			std::bind(&DeathZone::setFlag, this, std::placeholders::_1));

		return body;
	}

	void DeathZone::Update(const float deltaTime)
	{
		if (_madeContact)
		{
			_madeContact = false;
			static_cast<SceneLevel&>(_context.GetScene()).OnPlayerDied();
		}
	}
	void DeathZone::Draw(sf::RenderTarget& target, sf::RenderStates& states) const
	{

	}
	void DeathZone::Unload()
	{
		getContactManager().UnregisterBeginContact(_body->GetFixtureList(), _beginContactListener);
		Box2dObject::Unload();
	}

	void DeathZone::setFlag(b2Contact* contact)
	{
		_madeContact = true;
	}
}