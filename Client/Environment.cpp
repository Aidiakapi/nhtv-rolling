#include "stdafx.hpp"
#include "Environment.hpp"
#include "CollisionCategory.hpp"

namespace ak
{
	Environment::Environment(GameContext& context, const std::vector<b2Vec2> points, bool isLoop) :
		Box2dObject(context)
	{
		_vertArr = std::make_unique<sf::VertexArray>(sf::PrimitiveType::LinesStrip, points.size() + (isLoop ? 1 : 0));
		for (unsigned int i = 0; i < points.size(); i++)
		{
			(*_vertArr)[i].position = sf::Vector2f(points[i].x, points[i].y);
		}
		// If we're using a loop, finish it off
		if (isLoop) (*_vertArr)[points.size()].position = (*_vertArr)[0].position;

		_chain = std::make_unique<b2ChainShape>();
		(*_chain.*(isLoop ? &b2ChainShape::CreateLoop : &b2ChainShape::CreateChain))(&points[0], points.size());
	}

	Environment::~Environment() { }

	b2Body* Environment::CreateBody(b2World& world, ContactManager& contactManager)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2_staticBody;
		
		b2Body* body = world.CreateBody(&bodyDef);
		b2FixtureDef fix;
		fix.filter.categoryBits = CollisionCategory::Environment;
		fix.shape = _chain.get();
		fix.friction = 1.f;
		
		body->CreateFixture(&fix);

		//_chain = nullptr;

		return body;
	}

	void Environment::Update(const float deltaTime)
	{
	}

	void Environment::Draw(sf::RenderTarget& target, sf::RenderStates& states) const
	{
		target.draw(*_vertArr, states);
	}
}