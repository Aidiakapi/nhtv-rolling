#include "stdafx.hpp"
#include "Camera.hpp"

#include "Config.hpp"
#include "GameContext.hpp"
#include "SceneLevel.hpp"

namespace ak
{
	namespace
	{
		const sf::Uint32 clearFlag = 0x10000000;
	}

	Camera::Camera(GameContext& context) :
		GameObject(context, "Camera"),
		_trackObject(nullptr),
		_debugDraw(nullptr),
		_debugDrawFlags(b2Draw::e_shapeBit | b2Draw::e_jointBit | clearFlag)
	{
		SetDepth(-100000);
		Visible = false;
	}

	Camera::~Camera()
	{
	}

	void Camera::Load()
	{
		GameObject::Load();
		auto& scene = _context.GetScene();
		if (scene.Type != SceneType::Level)
			throw std::logic_error("Cannot instanciate Box2D objects in a non-level Scene.");
		_world = &static_cast<SceneLevel&>(_context.GetScene()).GetWorld();
	}

	void Camera::Update(const float deltaTime)
	{
		auto wndSize = static_cast<sf::Vector2f>(_context.GetWindow().getSize());
		if (_trackObject == nullptr)
		{
			_context.GetWindow().setView(sf::View(wndSize / 2.f, wndSize));
		}
		else
		{
			auto sceneHeight = static_cast<SceneLevel&>(_context.GetScene()).GetSize().y;
			auto objPos = _trackObject->getPosition() * PIXELS_PER_METER;
			_context.GetWindow().setView(sf::View(sf::Vector2f(objPos.x, sceneHeight - objPos.y - 2 * PIXELS_PER_METER), wndSize));
		}

		auto& input = _context.GetInput();
		if (input.IsPressed(Input::DebugDrawToggle)) Visible = !Visible;
		

		// Debug drawing of the Box2D world
		if (Visible)
		{
			if (_debugDraw.get() == nullptr)
			{
				_debugDraw = std::make_unique<DebugDraw>(*_world);
				_world->SetDebugDraw(_debugDraw.get());
			}

			if (input.IsPressed(Input::DebugDrawClear)) _debugDrawFlags ^= clearFlag; // Custom flag
			if (input.IsPressed(Input::DebugDrawShapes)) _debugDrawFlags ^= b2Draw::e_shapeBit;
			if (input.IsPressed(Input::DebugDrawJoints)) _debugDrawFlags ^= b2Draw::e_jointBit;
			if (input.IsPressed(Input::DebugDrawAabb)) _debugDrawFlags ^= b2Draw::e_aabbBit;
			if (input.IsPressed(Input::DebugDrawCenterOfMass)) _debugDrawFlags ^= b2Draw::e_centerOfMassBit;
			_debugDraw->SetFlags(_debugDrawFlags);
		}
		else
		{
			if (_debugDraw.get() != nullptr)
			{
				_debugDraw.reset();
				_world->SetDebugDraw(nullptr);
			}
		}
	}

	void Camera::Draw(sf::RenderTarget& target, sf::RenderStates& states) const
	{
		if (_debugDrawFlags & clearFlag) target.clear();
		sf::RenderStates transformedState(states);
		transformedState.transform.translate(0.f, static_cast<float>(static_cast<SceneLevel&>(_context.GetScene()).GetSize().y))
			.scale(PIXELS_PER_METER, -PIXELS_PER_METER);
		_debugDraw->SetTarget(&target, &transformedState);
		_world->DrawDebugData();
	}
}