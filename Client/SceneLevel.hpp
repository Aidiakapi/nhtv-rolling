#pragma once
#include "Scene.hpp"

namespace ak
{
	class ContactManager;
	class SceneLevel :
		public Scene
	{
	public:
		virtual ~SceneLevel() { };

		void Load() override;
		void HandleEvent(const sf::Event& event) override = 0;
		void Update(const float deltaTime) override;
		void Unload() override = 0;

		virtual const sf::Vector2i GetSize() const = 0;
		virtual void OnPlayerDied() = 0;

		inline b2World& GetWorld() { return *_world; }
		inline ContactManager& GetContactManager() { return *_contactManager; }
	protected:
		SceneLevel(GameContext& context);
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override = 0;

	private:
		b2World* _world;
		ContactManager* _contactManager;
	};
}