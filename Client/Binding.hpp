#pragma once

namespace ak
{
	class Binding final
	{
	public:
		enum class BindingType
		{
			Key,
			Mouse
		};
		const BindingType Type;
		const sf::Uint16 Value;

		inline Binding(sf::Keyboard::Key key) : Type(BindingType::Key), Value(key) { }
		inline Binding(sf::Mouse::Button mouseButton) : Type(BindingType::Mouse), Value(mouseButton) { }

		inline const bool operator==(const Binding other) const { return other.Type == Type && other.Value == Value; }
		inline const bool operator!=(const Binding other) const { return other.Type != Type || other.Value != Value; }
	};
}