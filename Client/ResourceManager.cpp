#include "stdafx.hpp"
#include "ResourceManager.hpp"

#define ASSET_FOLDER "..\\Assets\\"

namespace ak
{
	template<typename TSfml, typename ...Args>
	static const TSfml& ResourceManager::loadItem(const std::string name, ResCache<TSfml>& cache, Args&& ...arguments)
	{
		auto item = cache.find(name);
		if (item != cache.end()) return *(item->second.get());

		auto entry = std::make_unique<TSfml>();
		entry->loadFromFile(std::forward<Args>(arguments)...);

		auto copy = std::make_unique<const TSfml>(*entry);
		auto copyPtr = copy.get();

		cache.insert(std::make_pair(name, move(copy)));

		return *copyPtr;
	}

	const sf::Font& ResourceManager::GetFont(const std::string name)
	{
		std::string path = ASSET_FOLDER "Fonts\\";
		path.append(name).append(".ttf");
		return loadItem<sf::Font>(name, _fontCache, path);
	}

	const sf::Texture& ResourceManager::GetTexture(const std::string name)
	{
		std::string path = ASSET_FOLDER "Textures\\";
		path.append(name);
		return loadItem<sf::Texture>(name, _textureCache, path);
	}

	const sf::SoundBuffer& ResourceManager::GetSound(const std::string name)
	{
		std::string path = ASSET_FOLDER "Sounds\\";
		path.append(name);
		return loadItem<sf::SoundBuffer>(name, _soundCache, path);
	}
}

#undef ASSET_FOLDER