#include "stdafx.hpp"
#include "SceneLevel1.hpp"

#include "GameContext.hpp"
#include "InstanceManager.hpp"
#include "Config.hpp"

#include "SceneLevel2.hpp"

#include "Camera.hpp"
#include "Environment.hpp"
#include "Player.hpp"
#include "BouncePad.hpp"
#include "SimpleKey.hpp"
#include "SimpleDoor.hpp"

#include "LevelFinish.hpp"
#include "DeathZone.hpp"

namespace ak
{
	SceneLevel1::SceneLevel1(GameContext& context) :
		SceneLevel(context)
	{ }
	SceneLevel1::~SceneLevel1() { }

	namespace
	{
		template<typename TDoor, typename TKey>
		void createHouseWithKey(InstanceManager& inst,
			float x, float y, float angle,
			float width, float roofHeight, float roofOverlap,
			bool keySideways, sf::Color keyCol, sf::Color doorCol)
		{
			const float halfDoorW = 0.08f;
			const float halfW = width / 2.f;

			// Transformation to apply position and angle to the points
			sf::Transform tr;
			tr.translate(x, y);
			tr.rotate(angle);
			sf::Vector2f point;

			// Doors
			point = tr.transformPoint(-halfW + halfDoorW, 1.f);
			inst.CreateAt<TDoor>(point.x, point.y, angle, doorCol);
			point = tr.transformPoint(halfW - halfDoorW, 1.f);
			inst.CreateAt<TDoor>(point.x, point.y, angle, doorCol);

			// Key
			point = tr.transformPoint(0.f, 1.f);
			inst.CreateAt<TKey>(point.x, point.y, keySideways ? angle - 90.f : angle, keyCol);

			// Roof
			auto roofAngle = std::atan2(roofHeight, halfW);

			auto highY = 2.f + roofHeight;
			auto lowY = std::tan(roofAngle) * (halfW + roofOverlap);
			lowY = highY - lowY;

			std::vector<b2Vec2> env;
			point = tr.transformPoint(-halfW - roofOverlap, lowY);
			env.push_back(b2Vec2(point.x, point.y));
			point = tr.transformPoint(0, highY);
			env.push_back(b2Vec2(point.x, point.y));
			point = tr.transformPoint(halfW + roofOverlap, lowY);
			env.push_back(b2Vec2(point.x, point.y));
			inst.Create<Environment>(std::move(env), false);
		}
	}

	void SceneLevel1::Load()
	{
		SceneLevel::Load();
		auto& inst = _context.GetInstances();

		auto& player = inst.CreateAt<Player>(1.f, 16.86f, 0.f);

		auto& camera = inst.Create<Camera>();
		camera.SetTrackingObject(&player);

		// Level boundaries
		std::vector<b2Vec2> env;
		env.push_back(b2Vec2(0.f, 0.f));
		env.push_back(b2Vec2(0.f, 30.f));
		env.push_back(b2Vec2(95.14f, 30.f));
		env.push_back(b2Vec2(95.14f, 0.f));
		inst.Create<Environment>(std::move(env), true).Visible = false;

		// Main left part
		env.push_back(b2Vec2(0.f, 16.36f));
		env.push_back(b2Vec2(3.f, 16.36f));
		env.push_back(b2Vec2(3.48f, 15.5f));
		env.push_back(b2Vec2(3.84f, 14.82f));
		env.push_back(b2Vec2(4.22f, 13.96f));
		env.push_back(b2Vec2(4.58f, 12.86f));
		env.push_back(b2Vec2(4.9f, 11.64f));
		env.push_back(b2Vec2(5.44f, 9.08f));
		env.push_back(b2Vec2(5.7f, 7.9f));
		env.push_back(b2Vec2(5.98f, 6.9f));
		env.push_back(b2Vec2(6.36f, 5.78f));
		env.push_back(b2Vec2(6.72f, 5.02f));
		env.push_back(b2Vec2(7.16f, 4.44f));
		env.push_back(b2Vec2(7.62f, 4.f));
		env.push_back(b2Vec2(8.12f, 3.78f));
		env.push_back(b2Vec2(8.68f, 3.66f));
		env.push_back(b2Vec2(9.38f, 3.64f));
		env.push_back(b2Vec2(10.18f, 3.74f));
		env.push_back(b2Vec2(11.2f, 3.96f));
		env.push_back(b2Vec2(12.2f, 4.24f));
		env.push_back(b2Vec2(13.08f, 4.38f));
		env.push_back(b2Vec2(13.86f, 4.46f));
		env.push_back(b2Vec2(14.58f, 4.42f));
		env.push_back(b2Vec2(15.36f, 4.26f));
		env.push_back(b2Vec2(16.14f, 3.84f));
		env.push_back(b2Vec2(16.62f, 3.32f));
		env.push_back(b2Vec2(17.24f, 2.42f));
		env.push_back(b2Vec2(17.56f, 1.78f));
		env.push_back(b2Vec2(17.92f, 1.26f));
		env.push_back(b2Vec2(18.28f, 0.92f));
		env.push_back(b2Vec2(19.f, 0.52f));
		env.push_back(b2Vec2(19.84f, 0.28f));
		env.push_back(b2Vec2(20.88f, 0.18f));
		env.push_back(b2Vec2(21.44f, 0.22f));
		env.push_back(b2Vec2(22.f, 0.4f));
		env.push_back(b2Vec2(22.36f, 0.74f));
		env.push_back(b2Vec2(22.58f, 1.08f));
		env.push_back(b2Vec2(22.7f, 1.46f));
		env.push_back(b2Vec2(22.82f, 2.1f));
		env.push_back(b2Vec2(22.82f, 6.28f));
		env.push_back(b2Vec2(22.84f, 6.98f));
		env.push_back(b2Vec2(22.98f, 7.46f));
		env.push_back(b2Vec2(23.26f, 7.84f));
		env.push_back(b2Vec2(23.7f, 8.06f));
		env.push_back(b2Vec2(24.16f, 8.26f));
		env.push_back(b2Vec2(24.6f, 8.66f));
		env.push_back(b2Vec2(25.36f, 9.56f));
		env.push_back(b2Vec2(25.64f, 9.78f));
		env.push_back(b2Vec2(25.94f, 9.8f));
		env.push_back(b2Vec2(26.22f, 9.58f));
		env.push_back(b2Vec2(26.44f, 9.3f));
		env.push_back(b2Vec2(27.f, 8.52f));
		env.push_back(b2Vec2(27.36f, 8.28f));
		env.push_back(b2Vec2(27.86f, 8.14f));
		env.push_back(b2Vec2(28.44f, 8.12f));
		env.push_back(b2Vec2(30.08f, 8.12f));
		env.push_back(b2Vec2(30.6f, 8.04f));
		env.push_back(b2Vec2(31.08f, 7.88f));
		env.push_back(b2Vec2(31.4f, 7.7f));
		env.push_back(b2Vec2(31.72f, 7.4f));
		env.push_back(b2Vec2(32.16f, 6.92f));
		env.push_back(b2Vec2(32.56f, 6.7f));
		env.push_back(b2Vec2(32.82f, 6.64f));
		env.push_back(b2Vec2(34.02f, 6.64f));
		env.push_back(b2Vec2(34.36f, 6.58f));
		env.push_back(b2Vec2(34.7f, 6.44f));
		env.push_back(b2Vec2(34.98f, 6.2f));
		env.push_back(b2Vec2(35.36f, 5.76f));
		env.push_back(b2Vec2(35.74f, 5.5f));
		env.push_back(b2Vec2(36.22f, 5.32f));
		env.push_back(b2Vec2(36.76f, 5.2f));
		env.push_back(b2Vec2(37.46f, 5.14f));
		env.push_back(b2Vec2(38.32f, 5.16f));
		env.push_back(b2Vec2(38.88f, 5.26f));
		env.push_back(b2Vec2(39.3f, 5.46f));
		env.push_back(b2Vec2(39.46f, 5.7f));
		env.push_back(b2Vec2(39.54f, 5.96f));
		env.push_back(b2Vec2(39.76f, 6.16f));
		env.push_back(b2Vec2(40.f, 6.24f));
		env.push_back(b2Vec2(40.24f, 6.26f));
		env.push_back(b2Vec2(41.42f, 6.26f));
		env.push_back(b2Vec2(41.7f, 6.36f));
		env.push_back(b2Vec2(41.86f, 6.58f));
		env.push_back(b2Vec2(42.f, 6.82f));
		env.push_back(b2Vec2(42.36f, 7.2f));
		env.push_back(b2Vec2(42.72f, 7.4f));
		env.push_back(b2Vec2(43.08f, 7.52f));
		env.push_back(b2Vec2(43.48f, 7.62f));
		env.push_back(b2Vec2(44.46f, 7.88f));
		env.push_back(b2Vec2(44.8f, 8.04f));
		env.push_back(b2Vec2(45.12f, 8.24f));
		env.push_back(b2Vec2(45.42f, 8.56f));
		env.push_back(b2Vec2(45.76f, 8.8f));
		env.push_back(b2Vec2(46.24f, 9.02f));
		env.push_back(b2Vec2(46.76f, 9.18f));
		env.push_back(b2Vec2(47.22f, 9.3f));
		env.push_back(b2Vec2(48.32f, 9.48f));
		env.push_back(b2Vec2(50.2f, 9.7f));
		env.push_back(b2Vec2(51.32f, 9.7f));
		env.push_back(b2Vec2(51.98f, 9.86f));
		env.push_back(b2Vec2(52.56f, 10.12f));
		env.push_back(b2Vec2(53.12f, 10.34f));
		env.push_back(b2Vec2(53.66f, 10.42f));
		env.push_back(b2Vec2(55.28f, 10.42f));
		env.push_back(b2Vec2(55.88f, 10.28f));
		env.push_back(b2Vec2(56.5f, 10.02f));
		env.push_back(b2Vec2(56.82f, 9.78f));
		env.push_back(b2Vec2(57.06f, 9.54f));
		env.push_back(b2Vec2(58.02f, 8.52f));
		env.push_back(b2Vec2(58.2f, 8.32f));
		env.push_back(b2Vec2(58.42f, 8.16f));
		env.push_back(b2Vec2(60.4f, 6.72f));
		env.push_back(b2Vec2(60.96f, 6.3f));
		env.push_back(b2Vec2(61.38f, 5.9f));
		env.push_back(b2Vec2(61.76f, 5.48f));
		env.push_back(b2Vec2(62.16f, 4.94f));
		env.push_back(b2Vec2(62.66f, 4.1f));
		env.push_back(b2Vec2(63.08f, 3.2f));
		env.push_back(b2Vec2(64.f, 0.68f));
		inst.Create<Environment>(std::move(env), false);

		// Main right part
		env.push_back(b2Vec2(73.22f, 0.68f));
		env.push_back(b2Vec2(73.68f, 1.9f));
		env.push_back(b2Vec2(73.94f, 2.36f));
		env.push_back(b2Vec2(74.18f, 2.68f));
		env.push_back(b2Vec2(74.62f, 3.02f));
		env.push_back(b2Vec2(75.16f, 3.46f));
		env.push_back(b2Vec2(75.62f, 4.08f));
		env.push_back(b2Vec2(76.78f, 6.32f));
		env.push_back(b2Vec2(77.28f, 6.96f));
		env.push_back(b2Vec2(77.82f, 7.5f));
		env.push_back(b2Vec2(78.28f, 7.84f));
		env.push_back(b2Vec2(78.88f, 8.18f));
		env.push_back(b2Vec2(79.68f, 8.54f));
		env.push_back(b2Vec2(82.28f, 9.54f));
		env.push_back(b2Vec2(83.18f, 9.9f));
		env.push_back(b2Vec2(84.3f, 10.4f));
		env.push_back(b2Vec2(84.96f, 10.8f));
		env.push_back(b2Vec2(85.8f, 11.46f));
		env.push_back(b2Vec2(85.98f, 11.74f));
		env.push_back(b2Vec2(86.08f, 12.04f));
		env.push_back(b2Vec2(86.2f, 12.38f));
		env.push_back(b2Vec2(86.38f, 12.68f));
		env.push_back(b2Vec2(86.64f, 12.92f));
		env.push_back(b2Vec2(87.18f, 13.22f));
		env.push_back(b2Vec2(87.88f, 13.52f));
		env.push_back(b2Vec2(88.66f, 13.78f));
		env.push_back(b2Vec2(89.7f, 14.f));
		env.push_back(b2Vec2(90.62f, 14.02f));
		env.push_back(b2Vec2(92.72f, 13.86f));
		env.push_back(b2Vec2(93.44f, 13.84f));
		env.push_back(b2Vec2(94.22f, 13.86f));
		env.push_back(b2Vec2(95.14f, 13.92f));
		inst.Create<Environment>(std::move(env), false);

		// Floating island left (loop)
		env.push_back(b2Vec2(30.1f, 17.7f));
		env.push_back(b2Vec2(32.18f, 17.54f));
		env.push_back(b2Vec2(33.88f, 17.46f));
		env.push_back(b2Vec2(35.66f, 17.42f));
		env.push_back(b2Vec2(37.38f, 17.44f));
		env.push_back(b2Vec2(38.88f, 17.5f));
		env.push_back(b2Vec2(41.72f, 17.72f));
		env.push_back(b2Vec2(40.68f, 17.46f));
		env.push_back(b2Vec2(39.76f, 17.22f));
		env.push_back(b2Vec2(39.f, 16.82f));
		env.push_back(b2Vec2(38.34f, 16.34f));
		env.push_back(b2Vec2(37.8f, 15.66f));
		env.push_back(b2Vec2(37.12f, 14.44f));
		env.push_back(b2Vec2(36.76f, 13.72f));
		env.push_back(b2Vec2(36.36f, 13.16f));
		env.push_back(b2Vec2(35.92f, 12.82f));
		env.push_back(b2Vec2(35.56f, 12.64f));
		env.push_back(b2Vec2(34.2f, 12.02f));
		env.push_back(b2Vec2(33.76f, 11.7f));
		env.push_back(b2Vec2(33.62f, 11.46f));
		env.push_back(b2Vec2(33.64f, 11.22f));
		env.push_back(b2Vec2(33.8f, 10.96f));
		env.push_back(b2Vec2(34.36f, 10.36f));
		env.push_back(b2Vec2(34.48f, 10.14f));
		env.push_back(b2Vec2(34.4f, 9.92f));
		env.push_back(b2Vec2(34.22f, 9.78f));
		env.push_back(b2Vec2(32.64f, 9.22f));
		env.push_back(b2Vec2(32.32f, 9.16f));
		env.push_back(b2Vec2(31.94f, 9.18f));
		env.push_back(b2Vec2(31.64f, 9.32f));
		env.push_back(b2Vec2(31.44f, 9.54f));
		env.push_back(b2Vec2(31.3f, 9.92f));
		env.push_back(b2Vec2(31.22f, 10.56f));
		env.push_back(b2Vec2(31.24f, 11.38f));
		env.push_back(b2Vec2(31.4f, 13.78f));
		env.push_back(b2Vec2(31.36f, 15.22f));
		env.push_back(b2Vec2(31.24f, 15.74f));
		env.push_back(b2Vec2(31.14f, 16.1f));
		env.push_back(b2Vec2(31.f, 16.44f));
		env.push_back(b2Vec2(30.8f, 16.8f));
		env.push_back(b2Vec2(30.52f, 17.22f));
		inst.Create<Environment>(std::move(env), true);

		// Floating island right (loop)
		env.push_back(b2Vec2(60.06f, 17.6f));
		env.push_back(b2Vec2(61.5f, 17.8f));
		env.push_back(b2Vec2(62.74f, 17.94f));
		env.push_back(b2Vec2(64.16f, 18.08f));
		env.push_back(b2Vec2(65.4f, 18.18f));
		env.push_back(b2Vec2(66.9f, 18.24f));
		env.push_back(b2Vec2(69.16f, 18.26f));
		env.push_back(b2Vec2(70.64f, 18.16f));
		env.push_back(b2Vec2(72.48f, 18.02f));
		env.push_back(b2Vec2(75.86f, 17.6f));
		env.push_back(b2Vec2(74.6f, 17.44f));
		env.push_back(b2Vec2(72.74f, 17.26f));
		env.push_back(b2Vec2(71.22f, 17.08f));
		env.push_back(b2Vec2(70.06f, 16.92f));
		env.push_back(b2Vec2(68.56f, 16.66f));
		env.push_back(b2Vec2(67.18f, 16.36f));
		env.push_back(b2Vec2(66.24f, 16.14f));
		env.push_back(b2Vec2(65.46f, 16.f));
		env.push_back(b2Vec2(64.84f, 15.94f));
		env.push_back(b2Vec2(64.16f, 15.96f));
		env.push_back(b2Vec2(63.1f, 16.16f));
		env.push_back(b2Vec2(62.32f, 16.46f));
		env.push_back(b2Vec2(61.28f, 16.96f));
		inst.Create<Environment>(std::move(env), true);

		// Finish cover
		env.push_back(b2Vec2(89.84f, 16.14f));
		env.push_back(b2Vec2(90.58f, 16.26f));
		env.push_back(b2Vec2(91.2f, 16.26f));
		env.push_back(b2Vec2(91.6f, 16.24f));
		env.push_back(b2Vec2(92.1f, 16.16f));
		env.push_back(b2Vec2(93.44f, 15.9f));
		env.push_back(b2Vec2(94.28f, 15.9f));
		env.push_back(b2Vec2(95.14f, 16.04f));
		inst.Create<Environment>(std::move(env), false);

		// Bounce pads (left to right)
		inst.CreateAt<BouncePad>(20.7f, 0.17f, -2.f, 1.8f, 0.1f, 1.5f, 35.f, 50.f);
		inst.CreateAt<BouncePad>(48.18f, 9.44f, 9.2933f, 1.8f, 0.1f, 1.5f, 35.f, 50.f);

		// Colors for keys and doors
		auto col1 = sf::Color(255, 0, 255, 255);
		auto col2 = sf::Color(0, 255, 255, 255);
		auto col3 = sf::Color(128, 128, 255, 255);
		auto col4 = sf::Color(10, 255, 10, 255);
		auto col5 = sf::Color::Red;

		// Keys with houses (left to right)
		createHouseWithKey<SimpleDoor, SimpleKey>(inst, 13.96f, 4.38f, 0.f, 1.6f, 1.f, 0.5f, false, col1, col5);
		createHouseWithKey<SimpleDoor, SimpleKey>(inst, 38.74f, 17.48f, 3.2705f, 2.8f, 1.4f, 0.6f, false, col4, col3);
		createHouseWithKey<SimpleDoor, SimpleKey>(inst, 70.92f, 18.1f, -3.9909f, 2.8f, 1.2f, 0.7f, true, col5, col4);

		// Keys
		inst.CreateAt<SimpleKey>(28.02f, 8.88f, 0.f, col2);
		inst.CreateAt<SimpleKey>(75.08f, 4.46f, -26.1139f, col3);
		// 28.02f, 8.88f

		// Doors
		inst.CreateAt<SimpleDoor>(31.3f, 8.78f, 0.f, col2);
		inst.CreateAt<SimpleDoor>(89.88f, 15.06f, 0.f, col1);

		// Hole in ground
		inst.CreateAt<DeathZone>(63.f, 0.f, 0.f, sf::Vector2f(11.f, 0.68f));

		// Finish
		inst.CreateAt<LevelFinish<SceneLevel2>>(93.34f, 14.6f, 0.f);
	}

	void SceneLevel1::HandleEvent(const sf::Event& event)
	{
		switch (event.type)
		{
		case sf::Event::EventType::Closed:
			_context.GetWindow().close();
			break;
		}
	}

	void SceneLevel1::Update(const float deltaTime)
	{
		SceneLevel::Update(deltaTime);
		_context.GetInstances().Update(deltaTime);
	}

	void SceneLevel1::Unload()
	{

	}

	void SceneLevel1::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.clear();
		target.draw(_context.GetInstances(), states);
	}

	const sf::Vector2i SceneLevel1::GetSize() const
	{
		return sf::Vector2i(static_cast<sf::Int32>(96 * PIXELS_PER_METER), static_cast<sf::Int32>(30 * PIXELS_PER_METER));
	}

	void SceneLevel1::OnPlayerDied()
	{
		// Reload the scene
		_context.LoadScene<SceneLevel1>();
	}
}