#include "stdafx.hpp"
#include "ContactManager.hpp"

namespace ak
{
	ContactManager::ContactManager() :
		_nextIdentifier(0)
	{
	}

	sf::Uint32 ContactManager::RegisterBeginContact(b2Fixture* fixture, std::function<void(b2Contact*)> handler)
	{
		_beginContact.insert(std::make_pair(fixture, std::make_pair(++_nextIdentifier, handler)));
		return _nextIdentifier;
	}
	sf::Uint32 ContactManager::RegisterEndContact(b2Fixture* fixture, std::function<void(b2Contact*)> handler)
	{
		_endContact.insert(std::make_pair(fixture, std::make_pair(++_nextIdentifier, handler)));
		return _nextIdentifier;
	}
	sf::Uint32 ContactManager::RegisterPreSolve(b2Fixture* fixture, std::function<void(b2Contact*, const b2Manifold*)> handler)
	{
		_preSolve.insert(std::make_pair(fixture, std::make_pair(++_nextIdentifier, handler)));
		return _nextIdentifier;
	}
	sf::Uint32 ContactManager::RegisterPostSolve(b2Fixture* fixture, std::function<void(b2Contact*, const b2ContactImpulse*)> handler)
	{
		_postSolve.insert(std::make_pair(fixture, std::make_pair(++_nextIdentifier, handler)));
		return _nextIdentifier;
	}

	template<typename TMap>
	bool unregisterListener(TMap& map, b2Fixture* fixture, sf::Uint32 key)
	{
		auto range = map.equal_range(fixture);
		auto it = range.first;
		for (; it != range.second; ++it)
		{
			if (it->second.first == key) break;
		}
		if (it == range.second) return false;
		map.erase(it);
		return true;
	}

	bool ContactManager::UnregisterBeginContact(b2Fixture* fixture, sf::Uint32 key) { return unregisterListener(_beginContact, fixture, key); }
	bool ContactManager::UnregisterEndContact(b2Fixture* fixture, sf::Uint32 key) { return unregisterListener(_endContact, fixture, key); }
	bool ContactManager::UnregisterPreSolve(b2Fixture* fixture, sf::Uint32 key) { return unregisterListener(_preSolve, fixture, key); }
	bool ContactManager::UnregisterPostSolve(b2Fixture* fixture, sf::Uint32 key) { return unregisterListener(_postSolve, fixture, key); }

	template<typename TMap, typename ...Args>
	void callHandlers(TMap& map, b2Contact* contact, Args&& ...arguments)
	{
		auto range = map.equal_range(contact->GetFixtureA());
		for (auto it = range.first; it != range.second; ++it)
			it->second.second(contact, std::forward<Args>(arguments)...);

		range = map.equal_range(contact->GetFixtureB());
		for (auto it = range.first; it != range.second; ++it)
			it->second.second(contact, std::forward<Args>(arguments)...);
	}

	void ContactManager::BeginContact(b2Contact* contact)
	{
		callHandlers(_beginContact, contact);
	}
	void ContactManager::EndContact(b2Contact* contact)
	{
		callHandlers(_endContact, contact);
	}
	void ContactManager::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
	{
		callHandlers(_preSolve, contact, oldManifold);
	}
	void ContactManager::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
	{
		callHandlers(_postSolve, contact, impulse);
	}
}