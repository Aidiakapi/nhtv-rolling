#pragma once
#include "Box2dObject.hpp"

#include "GameContext.hpp"
#include "InstanceManager.hpp"
#include "ContactManager.hpp"
#include "CollisionCategory.hpp"
#include "Box2dSprite.hpp"

namespace ak
{
	template<class TNextScene>
	class LevelFinish :
		public Box2dObject
	{
	public:
		inline LevelFinish<TNextScene>(GameContext& context) :
			Box2dObject(context),
			_beginContactListener(0),
			_hasContact(false)
		{
			SetDepth(-6);
		}

		inline b2Body* CreateBody(b2World& world, ContactManager& contactManager) override
		{
			b2BodyDef bDef;
			bDef.type = b2_staticBody;
			bDef.position = b2Vec2(getPosition().x, getPosition().y);

			b2PolygonShape shp;
			shp.SetAsBox(0.5f, 0.75f);
			b2FixtureDef fDef;
			fDef.shape = &shp;
			fDef.filter.categoryBits = CollisionCategory::Entity;
			fDef.filter.maskBits = CollisionCategory::Player;
			fDef.isSensor = true;

			auto body = world.CreateBody(&bDef);
			auto fix = body->CreateFixture(&fDef);

			_beginContactListener = contactManager.RegisterBeginContact(fix,
				std::bind(&LevelFinish<TNextScene>::setFlag, this, std::placeholders::_1));
			
			return body;
		}

		inline void Load() override
		{
			auto sprite = std::make_unique<Box2dSprite>(
				_context.GetResources().GetTexture("Finish.png"));
			sprite->setOrigin(15.f, 22.5f);

			_sprite = std::move(sprite);

			Box2dObject::Load();
		}
		inline void Update(const float deltaTime) override
		{
			if (_hasContact)
				_context.LoadScene<TNextScene>();
		}
		inline void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override
		{
			target.draw(*_sprite, states);
		}
		inline void Unload() override
		{
			getContactManager().UnregisterBeginContact(_body->GetFixtureList(), _beginContactListener);
			Box2dObject::Unload();
		}

	private:
		inline void setFlag(b2Contact* contact)
		{
			_hasContact = true;
		}

		uptr<const Box2dSprite> _sprite;

		sf::Uint32 _beginContactListener;
		bool _hasContact;
	};
}