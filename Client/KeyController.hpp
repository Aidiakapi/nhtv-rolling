/*

Object that controls how many keys/doors there are of specific colors in a level.
It opens doors when all keys have been taken.

*/

#pragma once
#include "GameObject.hpp"
#include <unordered_map>

// Provide hasher for sf::Color
namespace std
{
	template<>
	struct hash<sf::Color>
	{
		inline size_t operator()(const sf::Color& obj) const
		{
			return std::hash<sf::Uint32>()(
				(static_cast<sf::Uint32>(obj.r) << 24) |
				(static_cast<sf::Uint32>(obj.g) << 16) |
				(static_cast<sf::Uint32>(obj.b) << 8) |
				(static_cast<sf::Uint32>(obj.a)));
		}
	};
}

namespace ak
{
	class ColoredDoor;
	class KeyController final :
		public GameObject
	{
	public:
		KeyController(GameContext& context);

		void AddKey(sf::Color color);
		void RemoveKey(sf::Color color);

		void AddDoor(ColoredDoor& door);
		bool RemoveDoor(ColoredDoor& door);

		void Load() override;
		void Update(const float deltaTime) override;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override;
		void Unload() override;
		
	private:
		std::unordered_map<sf::Color, sf::Uint16> _keyCounts;
		std::unordered_multimap<sf::Color, ColoredDoor* const> _doors;

		bool _unloaded;
	};
}