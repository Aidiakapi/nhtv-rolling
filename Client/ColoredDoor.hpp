#pragma once
#include "Box2dObject.hpp"

namespace ak
{
	class ColoredDoor :
		public Box2dObject
	{
	public:
		void Load() override;
		void Unload() override;

		sf::Color GetColor() const;

		virtual void OnOpened() = 0;
		virtual void OnClosed() = 0;
	protected:
		ColoredDoor(GameContext& context, sf::Color color, std::string name = "");

	private:
		const sf::Color _color;
	};
}