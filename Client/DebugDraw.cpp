#include "stdafx.hpp"
#include "DebugDraw.hpp"
#include <array>

namespace ak
{
	namespace
	{
		inline const sf::Color transformColor(const b2Color color)
		{
			return sf::Color(
				static_cast<sf::Uint8>(floor(color.r == 1.0 ? 255 : color.r * 256)),
				static_cast<sf::Uint8>(floor(color.g == 1.0 ? 255 : color.g * 256)),
				static_cast<sf::Uint8>(floor(color.b == 1.0 ? 255 : color.b * 256)),
				static_cast<sf::Uint8>(floor(color.a == 1.0 ? 255 : color.a * 256)));
		}

		inline const sf::Vector2f transformVec2(const b2Vec2 pos)
		{
			return sf::Vector2f(pos.x, pos.y);
		}

		inline sf::ConvexShape createPolygon(const b2Vec2* vertices, int32 vertexCount)
		{
			sf::ConvexShape shape;
			shape.setPointCount(vertexCount);
			for (int32 i = 0; i < vertexCount; ++i)
				shape.setPoint(i, transformVec2(vertices[i]));

			return shape;
		}
	}

	DebugDraw::DebugDraw(b2World& world)
	{

	}

	DebugDraw::~DebugDraw()
	{

	}
	
	void DebugDraw::SetTarget(sf::RenderTarget* const target, sf::RenderStates* const states) const
	{
		_target = target;
		_states = states;
	}

	void DebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
	{
		sf::Color col = transformColor(color);

		auto poly = createPolygon(vertices, vertexCount);
		poly.setOutlineColor(col);
		poly.setOutlineThickness(0.02f);
		poly.setFillColor(sf::Color::Transparent);

		_target->draw(poly, *_states);
	}

	void DebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
	{
		sf::Color col = transformColor(color);

		sf::ConvexShape poly = createPolygon(vertices, vertexCount);
		poly.setFillColor(sf::Color(col.r / 2U, col.g / 2U, col.b / 2U, 128));
		poly.setOutlineColor(col);
		poly.setOutlineThickness(0.02f);

		_target->draw(poly, *_states);
	}

	void DebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
	{
		sf::CircleShape shape(radius, 16U);
		shape.setPosition(center.x, center.y);
		shape.setOrigin(radius, radius);
		shape.setOutlineColor(transformColor(color));
		shape.setFillColor(sf::Color::Transparent);
		shape.setOutlineThickness(0.02f);

		_target->draw(shape, *_states);
	}
	void DebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
	{
		sf::Color col = transformColor(color);

		sf::CircleShape circle(radius, 16U);
		circle.setPosition(center.x, center.y);
		circle.setOrigin(radius, radius);

		circle.setOutlineThickness(0.02f);
		circle.setOutlineColor(col);
		circle.setFillColor(sf::Color(col.r / 2U, col.g / 2U, col.b / 2U, 128U));

		sf::RectangleShape line(sf::Vector2f(radius, 0.02f));
		line.setPosition(center.x, center.y);
		line.setOrigin(0.f, 0.01f);
		line.setRotation(RADTODEG(atan2(axis.y, axis.x)));

		line.setFillColor(col);

		_target->draw(circle, *_states);
		_target->draw(line, *_states);
	}
	void DebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
	{
		sf::Color col = transformColor(color);
		sf::VertexArray line(sf::PrimitiveType::Lines, 2U);
		line[0].position = transformVec2(p1);
		line[0].color = col;
		line[1].position = transformVec2(p2);
		line[1].color = col;

		_target->draw(line, *_states);
	}
	void DebugDraw::DrawTransform(const b2Transform& xf)
	{
		sf::VertexArray lines(sf::PrimitiveType::Lines, 4U);
		b2Vec2 p1 = xf.p, p2;
		lines[0].position = lines[2].position = transformVec2(xf.p);
		lines[1].position = transformVec2(xf.p + (0.3f * xf.q.GetXAxis()));
		lines[3].position = transformVec2(xf.p + (0.3f * xf.q.GetYAxis()));

		lines[0].color = lines[1].color = sf::Color::Red;
		lines[2].color = lines[3].color = sf::Color::Green;

		_target->draw(lines, *_states);
	}
}