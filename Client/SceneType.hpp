#pragma once

namespace ak
{
	enum class SceneType : sf::Uint8
	{
		Load,
		Menu,
		Level
	};
}