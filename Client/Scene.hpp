/*

The Scene is an isolated portion of the game that instanciates
GameObjects and controls the global rendering process.

A scene can be seen as the global container for 

Owned by a GameContext.

*/

#pragma once
#include "SceneType.hpp"

namespace ak
{
	class GameContext;
	class Scene :
		public sf::Drawable,
		public sf::NonCopyable
	{
	public:
		Scene(GameContext& context, SceneType type);

		const SceneType Type;

		virtual ~Scene() {};
		virtual void Load() = 0;
		virtual void HandleEvent(const sf::Event& event) = 0;
		virtual void Update(const float deltaTime) = 0;
		virtual void Unload() = 0;
	protected:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
		GameContext& _context;
	};
}