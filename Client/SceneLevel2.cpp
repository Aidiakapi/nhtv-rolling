#include "stdafx.hpp"
#include "SceneLevel2.hpp"

#include "GameContext.hpp"
#include "InstanceManager.hpp"
#include "Config.hpp"

#include "SceneLevel1.hpp"

#include "Camera.hpp"
#include "Environment.hpp"
#include "Player.hpp"
#include "BouncePad.hpp"
#include "SimpleKey.hpp"
#include "SimpleDoor.hpp"

#include "LevelFinish.hpp"
#include "DeathZone.hpp"

namespace ak
{
	SceneLevel2::SceneLevel2(GameContext& context) :
		SceneLevel(context)
	{ }
	SceneLevel2::~SceneLevel2() { }

	namespace
	{
		template<typename TDoor, typename TKey>
		void createHouseWithKey(InstanceManager& inst,
			float x, float y, float angle,
			float width, float roofHeight, float roofOverlap,
			bool keySideways, sf::Color keyCol, sf::Color doorCol)
		{
			const float halfDoorW = 0.08f;
			const float halfW = width / 2.f;

			// Transformation to apply position and angle to the points
			sf::Transform tr;
			tr.translate(x, y);
			tr.rotate(angle);
			sf::Vector2f point;

			// Doors
			point = tr.transformPoint(-halfW + halfDoorW, 1.f);
			inst.CreateAt<TDoor>(point.x, point.y, angle, doorCol);
			point = tr.transformPoint(halfW - halfDoorW, 1.f);
			inst.CreateAt<TDoor>(point.x, point.y, angle, doorCol);

			// Key
			point = tr.transformPoint(0.f, 1.f);
			inst.CreateAt<TKey>(point.x, point.y, keySideways ? angle - 90.f : angle, keyCol);

			// Roof
			auto roofAngle = std::atan2(roofHeight, halfW);

			auto highY = 2.f + roofHeight;
			auto lowY = std::tan(roofAngle) * (halfW + roofOverlap);
			lowY = highY - lowY;

			std::vector<b2Vec2> env;
			point = tr.transformPoint(-halfW - roofOverlap, lowY);
			env.push_back(b2Vec2(point.x, point.y));
			point = tr.transformPoint(0, highY);
			env.push_back(b2Vec2(point.x, point.y));
			point = tr.transformPoint(halfW + roofOverlap, lowY);
			env.push_back(b2Vec2(point.x, point.y));
			inst.Create<Environment>(std::move(env), false);
		}
	}

	void SceneLevel2::Load()
	{
		SceneLevel::Load();
		auto& inst = _context.GetInstances();

		auto& player = inst.CreateAt<Player>(1.f, 0.5f, 0.f);

		auto& camera = inst.Create<Camera>();
		camera.SetTrackingObject(&player);

		// Level boundaries
		std::vector<b2Vec2> env;
		env.push_back(b2Vec2(0.f, 0.f));
		env.push_back(b2Vec2(0.f, 37.5f));
		env.push_back(b2Vec2(72.f, 37.5f));
		env.push_back(b2Vec2(72.f, 0.f));
		inst.Create<Environment>(std::move(env), true).Visible = false;

		// Main env 1
		env.push_back(b2Vec2(0.f, 0.f));
		env.push_back(b2Vec2(3.f, 0.f));
		env.push_back(b2Vec2(3.f, 0.75f));
		env.push_back(b2Vec2(3.75f, 0.75f));
		env.push_back(b2Vec2(3.75f, 1.5f));
		env.push_back(b2Vec2(4.5f, 1.5f));
		env.push_back(b2Vec2(4.5f, 2.25f));
		env.push_back(b2Vec2(5.25f, 2.25f));
		env.push_back(b2Vec2(5.25f, 3.f));
		env.push_back(b2Vec2(6.f, 3.f));
		env.push_back(b2Vec2(6.f, 3.75f));
		env.push_back(b2Vec2(6.75f, 3.75f));
		env.push_back(b2Vec2(6.75f, 4.5f));
		env.push_back(b2Vec2(7.5f, 4.5f));
		env.push_back(b2Vec2(7.5f, 5.25f));
		env.push_back(b2Vec2(8.25f, 5.25f));
		env.push_back(b2Vec2(8.25f, 6.f));
		env.push_back(b2Vec2(9.f, 6.f));
		env.push_back(b2Vec2(9.f, 13.5f));
		env.push_back(b2Vec2(10.5f, 13.5f));
		env.push_back(b2Vec2(10.5f, 11.25f));
		env.push_back(b2Vec2(16.5f, 11.25f));
		env.push_back(b2Vec2(16.5f, 0.f));
		inst.Create<Environment>(std::move(env), false);

		// Main env 2
		env.push_back(b2Vec2(16.5f, 16.5f));
		env.push_back(b2Vec2(18.75f, 16.5f));
		env.push_back(b2Vec2(18.75f, 18.f));
		env.push_back(b2Vec2(20.25f, 18.f));
		env.push_back(b2Vec2(20.25f, 19.5f));
		env.push_back(b2Vec2(23.25f, 19.5f));
		env.push_back(b2Vec2(23.25f, 11.25f));
		env.push_back(b2Vec2(37.5f, 11.25f));
		env.push_back(b2Vec2(37.5f, 8.25f));
		env.push_back(b2Vec2(40.5f, 8.25f));
		env.push_back(b2Vec2(40.5f, 5.25f));
		env.push_back(b2Vec2(45.f, 5.25f));
		env.push_back(b2Vec2(45.f, 6.75f));
		env.push_back(b2Vec2(46.5f, 6.75f));
		env.push_back(b2Vec2(46.5f, 8.25f));
		env.push_back(b2Vec2(49.5f, 8.25f));
		env.push_back(b2Vec2(49.5f, 0.f));
		inst.Create<Environment>(std::move(env), false);

		// Main env 3
		env.push_back(b2Vec2(54.75f, 0.f));
		env.push_back(b2Vec2(54.75f, 6.f));
		env.push_back(b2Vec2(57.75f, 6.f));
		env.push_back(b2Vec2(57.75f, 5.25f));
		env.push_back(b2Vec2(59.25f, 5.25f));
		env.push_back(b2Vec2(59.25f, 4.5f));
		env.push_back(b2Vec2(61.5f, 4.5f));
		env.push_back(b2Vec2(61.5f, 2.25f));
		env.push_back(b2Vec2(72.f, 2.25f));
		env.push_back(b2Vec2(72.f, 25.f));
		inst.Create<Environment>(std::move(env), false);

		// Floating block above deathzone (loop)
		env.push_back(b2Vec2(19.5f, 11.5f));
		env.push_back(b2Vec2(20.5f, 11.5f));
		env.push_back(b2Vec2(20.5f, 10.5f));
		env.push_back(b2Vec2(19.5f, 10.5f));
		inst.Create<Environment>(std::move(env), true);

		// Floating env 1, bottom
		env.push_back(b2Vec2(30.f, 23.25f));
		env.push_back(b2Vec2(31.5f, 23.25f));
		env.push_back(b2Vec2(31.5f, 21.75f));
		env.push_back(b2Vec2(33.75f, 21.75f));
		env.push_back(b2Vec2(33.75f, 19.5f));
		env.push_back(b2Vec2(36.f, 19.5f));
		env.push_back(b2Vec2(36.f, 17.25f));
		env.push_back(b2Vec2(39.f, 17.25f));
		env.push_back(b2Vec2(39.f, 19.5f));
		env.push_back(b2Vec2(45.f, 19.5f));
		inst.Create<Environment>(std::move(env), false);

		// Floating env 1, top
		env.push_back(b2Vec2(45.f, 21.5f));
		env.push_back(b2Vec2(42.75f, 21.5f));
		env.push_back(b2Vec2(42.75f, 23.75f));
		env.push_back(b2Vec2(40.5f, 23.75f));
		env.push_back(b2Vec2(40.5f, 26.f));
		env.push_back(b2Vec2(38.25f, 26.f));
		env.push_back(b2Vec2(38.25f, 28.25f));
		env.push_back(b2Vec2(36.f, 28.25f));
		env.push_back(b2Vec2(36.f, 30.5f));
		inst.Create<Environment>(std::move(env), false);

		// Floating env 2
		env.push_back(b2Vec2(45.75f, 15.f));
		env.push_back(b2Vec2(48.f, 15.f));
		env.push_back(b2Vec2(48.f, 12.75f));
		env.push_back(b2Vec2(50.25f, 12.75f));
		env.push_back(b2Vec2(50.25f, 12.f));
		env.push_back(b2Vec2(51.75f, 12.f));
		env.push_back(b2Vec2(51.75f, 11.25f));
		env.push_back(b2Vec2(54.f, 11.25f));
		env.push_back(b2Vec2(54.f, 13.5f));
		env.push_back(b2Vec2(57.f, 13.5f));
		env.push_back(b2Vec2(57.f, 12.f));
		env.push_back(b2Vec2(59.25f, 12.f));
		env.push_back(b2Vec2(59.25f, 15.f));
		env.push_back(b2Vec2(69.f, 15.f));
		env.push_back(b2Vec2(69.f, 18.f));
		env.push_back(b2Vec2(66.f, 18.f));
		env.push_back(b2Vec2(66.f, 17.f));
		inst.Create<Environment>(std::move(env), false);

		// Bounce pad supports
		env.push_back(b2Vec2(1.f, 8.5f));
		env.push_back(b2Vec2(2.f, 7.5f));
		inst.Create<Environment>(std::move(env), false);

		// Bounce pads (left to right)
		inst.CreateAt<BouncePad>(1.5f, 8.f, -45.f, 1.4142f, 0.1f, 1.15f, 40.f, 50.f);
		inst.CreateAt<BouncePad>(24.f, 11.2f, 0.f, 1.5f, 0.1f, 1.15f, 30.f, 35.f);

		// Death zones
		inst.CreateAt<DeathZone>(16.5f, 0.f, 0.f, sf::Vector2f(38.25f, 0.75f));

		// Colors for doors and keys
		auto col1 = sf::Color::Red;
		auto col2 = sf::Color(0, 255, 255, 255);
		auto col3 = sf::Color(128, 128, 255, 255);
		auto col4 = sf::Color(255, 0, 255, 255);

		// Houses
		createHouseWithKey<SimpleDoor, SimpleKey>(inst, 27.f, 11.25f, 0.f, 1.5f, 0.5f, 0.5f, false, col4, col3);
		createHouseWithKey<SimpleDoor, SimpleKey>(inst, 39.75f, 8.25f, 0.f, 1.5f, 0.8f, 0.3f, false, col3, col2);

		// Keys
		inst.CreateAt<SimpleKey>(20.f, 12.f, -90.f, col1);
		inst.CreateAt<SimpleKey>(32.625f, 22.25f, 90.f, col2).setScale(-1.f, 1.f);

		inst.CreateAt<SimpleKey>(56.f, 6.75f, 0.f, col1);

		// Doors
		inst.CreateAt<SimpleDoor>(30.08f, 24.25f, 0.f, col1);
		inst.CreateAt<SimpleDoor>(43.875, 20.5f, 0.f, col2);
		inst.CreateAt<SimpleDoor>(66.f, 16.f, 0.f, col4);

		// Finish
		inst.CreateAt<LevelFinish<SceneLevel1>>(67.5f, 15.75f, 0.f);
	}

	void SceneLevel2::HandleEvent(const sf::Event& event)
	{
		switch (event.type)
		{
		case sf::Event::EventType::Closed:
			_context.GetWindow().close();
			break;
		}
	}

	void SceneLevel2::Update(const float deltaTime)
	{
		SceneLevel::Update(deltaTime);
		_context.GetInstances().Update(deltaTime);
	}

	void SceneLevel2::Unload()
	{

	}

	void SceneLevel2::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.clear();
		target.draw(_context.GetInstances(), states);
	}

	const sf::Vector2i SceneLevel2::GetSize() const
	{
		return sf::Vector2i(static_cast<sf::Int32>(72 * PIXELS_PER_METER), static_cast<sf::Int32>(37.5f * PIXELS_PER_METER));
	}

	void SceneLevel2::OnPlayerDied()
	{
		// Reload the scene
		_context.LoadScene<SceneLevel2>();
	}
}