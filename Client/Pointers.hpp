#pragma once

namespace ak {
	// Define an alias to std::unique_ptr and std::shared_ptr
	template<typename T>
	using uptr = std::unique_ptr<T>;
	template<typename T>
	using sptr = std::shared_ptr<T>;
}