#include "stdafx.hpp"
#include "GameContext.hpp"
#include "Scene.hpp"

namespace ak {
	GameContext::GameContext(uptr<sf::RenderWindow> wnd) :
		_wnd(std::move(wnd)),
		_instances(std::make_unique<InstanceManager>(*this)),
		_resources(std::make_unique<ResourceManager>()),
		_input(std::make_unique<InputManager>(*_wnd)),

		_scene(nullptr),
		_nextScene(nullptr),
		_isUpdating(false)
	{
	}

	void GameContext::loadScene(uptr<Scene> scene)
	{
		if (_scene != nullptr)
		{
			_scene->Unload();
			_instances->Clean();
		}
		_scene = std::move(scene);
		_scene->Load();
	}

	void GameContext::Update(const float deltaTime)
	{
		_isUpdating = true;
		_input->Update(*_scene);
		_scene->Update(deltaTime);
		_isUpdating = false;
		if (_nextScene != nullptr) loadScene(std::move(_nextScene));
	}

	void GameContext::Draw() const
	{
		_wnd->draw(*_scene);
		_wnd->display();
	}

	InstanceManager& GameContext::GetInstances() const { return *_instances; }
	ResourceManager& GameContext::GetResources() const { return *_resources; }
	InputManager& GameContext::GetInput() const { return *_input; }

	sf::RenderWindow& GameContext::GetWindow() const { return *_wnd; }
}