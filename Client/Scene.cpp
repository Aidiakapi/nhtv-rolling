#include "stdafx.hpp"
#include "Scene.hpp"

namespace ak
{
	Scene::Scene(GameContext& context, SceneType type) :
		_context(context),
		Type(type)
	{ }
}