/*

See the notes in the header file.

*/

#include "stdafx.hpp"
#include "Box2dSprite.hpp"

#include "Config.hpp"

namespace ak
{
	Box2dSprite::Box2dSprite() :
		m_texture(nullptr),
		m_textureRect()
	{
	}

	Box2dSprite::Box2dSprite(const sf::Texture& texture) :
		m_texture(nullptr),
		m_textureRect()
	{
		setTexture(texture);
	}

	Box2dSprite::Box2dSprite(const sf::Texture& texture, const sf::IntRect& rectangle) :
		m_texture(nullptr),
		m_textureRect()
	{
		setTexture(texture);
		setTextureRect(rectangle);
	}

	void Box2dSprite::setTexture(const sf::Texture& texture, bool resetRect)
	{
		// Recompute the texture area if requested, or if there was no valid texture & rect before
		if (resetRect || (!m_texture && (m_textureRect == sf::IntRect())))
			setTextureRect(sf::IntRect(0, 0, texture.getSize().x, texture.getSize().y));

		// Assign the new texture
		m_texture = &texture;
	}

	void Box2dSprite::setTextureRect(const sf::IntRect& rectangle)
	{
		if (rectangle != m_textureRect)
		{
			m_textureRect = rectangle;
			updatePositions();
			updateTexCoords();
		}
	}

	void Box2dSprite::setColor(const sf::Color& color)
	{
		// Update the vertices' color
		m_vertices[0].color = color;
		m_vertices[1].color = color;
		m_vertices[2].color = color;
		m_vertices[3].color = color;
	}

	const sf::Texture* Box2dSprite::getTexture() const
	{
		return m_texture;
	}

	const sf::IntRect& Box2dSprite::getTextureRect() const
	{
		return m_textureRect;
	}

	const sf::Color& Box2dSprite::getColor() const
	{
		return m_vertices[0].color;
	}

	sf::FloatRect Box2dSprite::getLocalBounds() const
	{
		float width = static_cast<float>(std::abs(m_textureRect.width));
		float height = static_cast<float>(std::abs(m_textureRect.height));

		return sf::FloatRect(0.f, 0.f, width, height);
	}

	sf::FloatRect Box2dSprite::getGlobalBounds() const
	{
		return sf::Transform()
			.scale(1 / PIXELS_PER_METER, 1 / -PIXELS_PER_METER)
			.combine(getTransform())
			.transformRect(getLocalBounds());
	}

	void Box2dSprite::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		if (m_texture)
		{
			states.transform.scale(1 / PIXELS_PER_METER, 1 / -PIXELS_PER_METER);
			states.transform *= getTransform();
			states.texture = m_texture;
			target.draw(m_vertices, 4, sf::Quads, states);
		}
	}

	void Box2dSprite::updatePositions()
	{
		sf::FloatRect bounds = getLocalBounds();

		m_vertices[0].position = sf::Vector2f(0, 0);
		m_vertices[1].position = sf::Vector2f(0, bounds.height);
		m_vertices[2].position = sf::Vector2f(bounds.width, bounds.height);
		m_vertices[3].position = sf::Vector2f(bounds.width, 0);
	}

	void Box2dSprite::updateTexCoords()
	{
		float left = static_cast<float>(m_textureRect.left);
		float right = left + m_textureRect.width;
		float top = static_cast<float>(m_textureRect.top);
		float bottom = top + m_textureRect.height;

		m_vertices[0].texCoords = sf::Vector2f(left, top);
		m_vertices[1].texCoords = sf::Vector2f(left, bottom);
		m_vertices[2].texCoords = sf::Vector2f(right, bottom);
		m_vertices[3].texCoords = sf::Vector2f(right, top);
	}
}