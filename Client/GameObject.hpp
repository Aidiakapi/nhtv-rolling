/*

GameObjects contain most of the logic in a game.

The transformation is based on sf::Transformable.
Internally this uses a plain 3x3 matrix to represent
the most important 2D transformations. (Translate,
rotate and scale.)

Additionally a GameObject can be named, which allows
it to be accessed quickly through the InstanceManager.
This can be used to communicate with other objects.

GameObjects also implement sf::Drawable which allows
the GameObjects to be directly drawn to a RenderTarget.

Owned by InstanceManager.

*/

#pragma once
#include <atomic>

namespace ak
{
	class InstanceManager;
	class GameContext;
	class GameObject :
		public sf::Drawable,
		public sf::Transformable,
		public sf::NonCopyable
	{
		friend struct std::hash<GameObject>;
		friend class InstanceManager;
	public:
		virtual ~GameObject();

		inline sf::Uint32 GetID() const { return _id; }
		bool IsNamed() const;
		const std::string& GetName() const;

		// Active determines whether Update and/or Draw are called
		inline bool IsActive() const { return _active; }
		void Activate();
		void Deactivate();

		// Visible determines whether Draw is called (but only if active)
		bool Visible;

		// Persistent objects aren't cleaned by the InstanceManager, as a
		// result they remain active across Scenes. Can be set at any time
		// but only has an impact when the InstanceManager is cleaned.
		bool Persistent;

		// Depth is broadcasted to the InstanceManager which maintains
		// a list sorted by draw order.
		inline sf::Int32 GetDepth() const { return _depth; }
		void SetDepth(sf::Int32 depth);

		virtual void Load();
		virtual void Unload();
		virtual void Update(const float deltaTime) = 0;
		virtual void Draw(sf::RenderTarget& target, sf::RenderStates& states) const = 0;
	protected:
		GameObject(GameContext& context, std::string name = "");

		GameContext& _context;

		// sf::Drawable
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	private:
		bool _active;

		const sf::Uint32 _id;
		const std::string _name;

		sf::Int32 _depth;

		// Used to get new _id's
		static std::atomic<sf::Uint32> _nextId;
	};
}

namespace std
{
	template <>
	struct std::hash<ak::GameObject>
	{
		inline size_t operator()(const ak::GameObject& obj) const
		{
			return std::hash<sf::Uint32>()(obj._id);
		}
	};
}