#include "stdafx.hpp"
#include "SceneLevel.hpp"

#include "GameContext.hpp"
#include "InstanceManager.hpp"
#include "ObjectWorld.hpp"

#include "KeyController.hpp"

namespace ak
{
	SceneLevel::SceneLevel(GameContext& context) :
		Scene(context, SceneType::Level)
	{ }

	void SceneLevel::Load()
	{
		// Box2D World
		auto& worldObj = _context.GetInstances().Create<ObjectWorld>();
		_world = worldObj.GetWorld();
		_contactManager = worldObj.GetContactManager();

		// Key controller (controls colored keys and doors)
		_context.GetInstances().Create<KeyController>();
	}

	void SceneLevel::Update(const float deltaTime)
	{
		_world->Step(deltaTime, 8, 4);
	}
}