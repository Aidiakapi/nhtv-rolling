#pragma once

namespace ak
{
	class CollisionCategory
	{
	public:
		enum : uint16
		{
			None = 0x0000U,
			Environment = 0x0002U,
			EnvironmentProp = 0x0004U,
			Entity = 0x0008U,
			Player = 0x0010U,
			All = 0xFFFFU
		};
	};
}