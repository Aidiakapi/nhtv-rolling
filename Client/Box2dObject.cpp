#include "stdafx.hpp"
#include "Box2dObject.hpp"

#include "Config.hpp"
#include "SceneLevel.hpp"
#include "GameContext.hpp"
#include "InstanceManager.hpp"

namespace ak
{
	Box2dObject::Box2dObject(GameContext& context, std::string name) :
		GameObject(context, name)
	{ }

	void Box2dObject::Load()
	{
		GameObject::Load();
		auto& scene = _context.GetScene();
		if (scene.Type != SceneType::Level)
			throw std::logic_error("Cannot instanciate Box2D objects in a non-level Scene.");

		_body = CreateBody(static_cast<SceneLevel&>(scene).GetWorld(),
			static_cast<SceneLevel&>(scene).GetContactManager());
	}

	void Box2dObject::Unload()
	{
		if (_body != nullptr)
			static_cast<SceneLevel&>(_context.GetScene()).GetWorld().DestroyBody(_body);
		GameObject::Unload();
	}

	void Box2dObject::Update(const float deltaTime)
	{
		if (_body != nullptr && _body->IsAwake())
		{
			auto pos = _body->GetPosition();
			setPosition(pos.x, pos.y);
			setRotation(_body->GetAngle() * 180.f / b2_pi);
		}
	}

	void Box2dObject::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform.translate(0.f, static_cast<float>(static_cast<SceneLevel&>(_context.GetScene()).GetSize().y))
			.scale(PIXELS_PER_METER, -PIXELS_PER_METER)
			.combine(getTransform());
		Draw(target, states);
	}

	b2World& Box2dObject::getWorld()
	{
		return static_cast<SceneLevel&>(_context.GetScene()).GetWorld();
	}
	const b2World& Box2dObject::getWorld() const
	{
		return static_cast<SceneLevel&>(_context.GetScene()).GetWorld();
	}
	ContactManager& Box2dObject::getContactManager()
	{
		return static_cast<SceneLevel&>(_context.GetScene()).GetContactManager();
	}
}