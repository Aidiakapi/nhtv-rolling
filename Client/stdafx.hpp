#pragma once

// Includes from standard
#include <memory>
#include <stdexcept>
#include <functional>
#include <iostream>

// SFML
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

// Box2D
#include <Box2D/Box2D.h>

// Our own headers
#include "Pointers.hpp"
#include "Angles.hpp"