#include "stdafx.hpp"
#include "SimpleDoor.hpp"

#include "GameContext.hpp"
#include "ResourceManager.hpp"
#include "CollisionCategory.hpp"

namespace ak
{
	SimpleDoor::SimpleDoor(GameContext& context, sf::Color color) :
		ColoredDoor(context, color)
	{
		SetDepth(-4);
	}

	b2Body* SimpleDoor::CreateBody(b2World& world, ContactManager& contactManager)
	{
		b2BodyDef bDef;
		bDef.type = b2_staticBody;
		bDef.position = b2Vec2(getPosition().x, getPosition().y);
		bDef.angle = DEGTORAD(getRotation());

		b2PolygonShape shp;
		shp.SetAsBox(0.08f, 1.f);
		b2FixtureDef fDef;
		fDef.shape = &shp;
		fDef.filter.categoryBits = CollisionCategory::EnvironmentProp;

		auto body = world.CreateBody(&bDef);
		body->CreateFixture(&fDef);
		return body;
	}

	void SimpleDoor::Load()
	{
		ColoredDoor::Load();
		auto sprite = std::make_unique<Box2dSprite>(_context.GetResources().GetTexture("Door_Frame.png"));
		sprite->setOrigin(22.f, 30.f);
		sprite->setColor(GetColor());
		_frame = std::move(sprite);

		sprite = std::make_unique<Box2dSprite>(_context.GetResources().GetTexture("Door_Slider.png"));
		sprite->setOrigin(22.f, 30.f);
		sprite->setColor(GetColor());

		_slider = std::move(sprite);
	}

	void SimpleDoor::Update(const float deltaTime)
	{
		// It's a static object, so don't call Box2dObject's update function
	}
	void SimpleDoor::Draw(sf::RenderTarget& target, sf::RenderStates& states) const
	{
		target.draw(*_frame, states);
		if (_body)
			target.draw(*_slider, states);
	}

	void SimpleDoor::OnOpened()
	{
		if (!_body) return;
		auto body = _body;
		_body = nullptr;
		getWorld().DestroyBody(body);
	}

	void SimpleDoor::OnClosed()
	{
		if (_body) return;
		_body = CreateBody(getWorld(), getContactManager());
	}
}