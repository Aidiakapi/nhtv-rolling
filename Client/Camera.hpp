/*

The camera objects tracks a specific object in the Box2D world, and renders it.
This should be the last processed object in the Box2D world.

It also controls the Box2D debug drawing.
To smoothly track an object's position the camera is processed after the Box2D
step has occurred. Because of this the debug drawing is one step ahead of the
currently processed frame and may seem slightly offset. However, this shouldn't
be a problem in general.

*/

#pragma once
#include "GameObject.hpp"
#include "DebugDraw.hpp"

namespace ak
{
	class Camera :
		public GameObject
	{
	public:
		Camera(GameContext& context);
		virtual ~Camera();

		void Load() override;
		void Update(const float deltaTime) override;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override;

		inline void SetTrackingObject(const GameObject* trackObject) { _trackObject = trackObject; }
		inline const GameObject* GetTrackingObject() { return _trackObject; }

	private:
		const GameObject* _trackObject;
		b2World* _world;
		uptr<DebugDraw> _debugDraw;

		uint32 _debugDrawFlags;
	};
}