#pragma once
#include "ColoredKey.hpp"
#include "Box2dSprite.hpp"

namespace ak
{
	class SimpleKey :
		public ColoredKey
	{
	public:
		SimpleKey(GameContext& context, sf::Color color);

		b2Body* CreateBody(b2World& world, ContactManager& contactManager) override;

		void Load() override;
		void Update(const float deltaTime) override;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override;
		void Unload() override;
	private:
		uptr<const Box2dSprite> _sprite;

		void handleBeginContact(b2Contact* contact);

		sf::Uint32 _beginContact;
		bool _hasContact;
	};
}