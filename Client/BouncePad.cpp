#include "stdafx.hpp"
#include "BouncePad.hpp"
#include "CollisionCategory.hpp"

namespace ak
{
	BouncePad::BouncePad(GameContext& context, float width, float height, float distance, float force, float speed) :
		Box2dObject(context),
		_width(width),
		_height(height),
		_dist(distance),
		_force(force),
		_speed(speed)
	{ }
	BouncePad::~BouncePad() { }

	b2Body* BouncePad::CreateBody(b2World& world, ContactManager& contactManager)
	{
		float angle = DEGTORAD(getRotation());

		b2BodyDef bDefBase;
		bDefBase.type = b2_staticBody;
		bDefBase.position.Set(getPosition().x, getPosition().y);
		bDefBase.angle = angle;

		b2Body* bBase = world.CreateBody(&bDefBase);

		b2FixtureDef fDefBase;
		b2PolygonShape shpBase;
		shpBase.SetAsBox(_width / 2.f, _height / 2.f);
		fDefBase.shape = &shpBase;
		fDefBase.filter.categoryBits = CollisionCategory::None;

		b2Fixture* fBase = bBase->CreateFixture(&fDefBase);

		b2BodyDef bDefPad;
		bDefPad.type = b2_dynamicBody;
		bDefPad.linearDamping = 0.1f;
		bDefPad.fixedRotation = true;

		// Set the actual pad perpendicular to the base
		bDefPad.position = bDefBase.position + b2Vec2(std::cos(angle + b2_pi / 2.f) * _dist, std::sin(angle + b2_pi / 2.f) * _dist);
		bDefPad.angle = angle;

		b2Body* bPad = world.CreateBody(&bDefPad);

		fDefBase.density = 0.5f;
		fDefBase.restitution = 0.5f;
		fDefBase.filter.categoryBits = CollisionCategory::EnvironmentProp;
		// Don't collide with the environment
		fDefBase.filter.maskBits ^= CollisionCategory::Environment;
		b2Fixture* fPad = bPad->CreateFixture(&fDefBase);

		b2PrismaticJointDef jointDef;
		jointDef.bodyA = bBase;
		jointDef.bodyB = bPad;
		jointDef.collideConnected = true;
		jointDef.localAxisA = b2Vec2(0.f, 1.f);

		jointDef.localAnchorA = jointDef.localAnchorB = b2Vec2_zero;
		jointDef.enableMotor = true;
		jointDef.enableLimit = true;
		jointDef.lowerTranslation = 0.f;
		jointDef.upperTranslation = _dist;
		jointDef.motorSpeed = _speed;
		jointDef.maxMotorForce = _force;

		b2PrismaticJoint* joint = static_cast<b2PrismaticJoint*>(world.CreateJoint(&jointDef));
		
		_baseBody = bBase;
		return bPad;
	}

	void BouncePad::Update(const float deltaTime)
	{
		Box2dObject::Update(deltaTime);
	}

	void BouncePad::Draw(sf::RenderTarget& target, sf::RenderStates& states) const
	{
		sf::RectangleShape rect(sf::Vector2f(_width, _height));
		rect.setFillColor(sf::Color::Yellow);
		rect.setOrigin(_width / 2.f, _height / 2.f);

		target.draw(rect, states);

		auto offset = _width / 4.f;
		auto col = sf::Color(128, 128, 255, 255);
		sf::VertexArray varr(sf::PrimitiveType::Lines, 2);
		varr.append(sf::Vertex(sf::Vector2f(0.f, 0.f), col));

		auto relPos = _baseBody->GetPosition() - _body->GetPosition();
		float dist = std::sqrt(relPos.x * relPos.x + relPos.y * relPos.y);
		float scaleFac = (dist - _height) / dist;
		varr.append(sf::Vertex(sf::Vector2f(relPos.x * scaleFac, relPos.y * scaleFac), col));

		states.transform.translate(sf::Vector2f(-offset, -_height / 2.f));
		states.transform.rotate(-getRotation());
		target.draw(varr, states);
		states.transform.rotate(getRotation());
		states.transform.translate(sf::Vector2f(offset * 2, 0.f));
		states.transform.rotate(-getRotation());
		target.draw(varr, states);
	}
	
	void BouncePad::Unload()
	{
		// Unloads dynamic body and joint
		Box2dObject::Unload();
		getWorld().DestroyBody(_baseBody);
	}
}