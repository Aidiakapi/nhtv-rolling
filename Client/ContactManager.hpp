#pragma once
#include <unordered_map>

namespace ak
{
	class ContactManager final :
		private b2ContactListener,
		sf::NonCopyable
	{
		friend class ObjectWorld;
	public:
		ContactManager();

		sf::Uint32 RegisterBeginContact(b2Fixture* fixture, std::function<void(b2Contact*)> handler);
		sf::Uint32 RegisterEndContact(b2Fixture* fixture, std::function<void(b2Contact*)> handler);
		sf::Uint32 RegisterPreSolve(b2Fixture* fixture, std::function<void(b2Contact*, const b2Manifold*)> handler);
		sf::Uint32 RegisterPostSolve(b2Fixture* fixture, std::function<void(b2Contact*, const b2ContactImpulse*)> handler);

		bool UnregisterBeginContact(b2Fixture* fixture, sf::Uint32 key);
		bool UnregisterEndContact(b2Fixture* fixture, sf::Uint32 key);
		bool UnregisterPreSolve(b2Fixture* fixture, sf::Uint32 key);
		bool UnregisterPostSolve(b2Fixture* fixture, sf::Uint32 key);

	private:
		sf::Uint32 _nextIdentifier;

		typedef std::unordered_multimap<b2Fixture*, std::pair<sf::Uint32, std::function<void(b2Contact*)>>> ContactListeners;
		typedef std::unordered_multimap<b2Fixture*, std::pair<sf::Uint32, std::function<void(b2Contact*, const b2Manifold*)>>> PreSolveListeners;
		typedef std::unordered_multimap<b2Fixture*, std::pair<sf::Uint32, std::function<void(b2Contact*, const b2ContactImpulse* impulse)>>> PostSolveListeners;
		ContactListeners _beginContact;
		ContactListeners _endContact;
		PreSolveListeners _preSolve;
		PostSolveListeners _postSolve;

		// b2ContactListener
		void BeginContact(b2Contact* contact) override;
		void EndContact(b2Contact* contact) override;
		void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override;
		void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) override;
	};
}