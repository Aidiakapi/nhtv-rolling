// Auto-generated. Changes will be overwritten when compiled.
#pragma once
#include "stdafx.hpp"
#include <unordered_map>

namespace ak
{
    class Input
    {
    public:
        enum InputType : sf::Uint16
        {
            MoveLeft = 1,
            MoveRight = 2,
            Jump = 3,
            SpawnObject = 4,
            Reset = 5,
            DebugDrawToggle = 6,
            DebugDrawClear = 7,
            DebugDrawShapes = 8,
            DebugDrawJoints = 9,
            DebugDrawAabb = 10,
            DebugDrawCenterOfMass = 11
        };

        static const InputType GetType(const std::string& name);
        static const std::string& GetName(const InputType input);
        static const sf::Uint16 Count;
        static const Input Items;
        const std::string* begin() const;
        const std::string* end() const;

    private:
        static const std::unordered_map<std::string, const InputType> _valueMap;
        static const std::string* const _names;

        inline Input() { }
    };
}
