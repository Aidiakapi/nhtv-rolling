#include "stdafx.hpp"
#include "KeyController.hpp"

#include "ColoredDoor.hpp"

namespace ak
{
	KeyController::KeyController(GameContext& context) :
		GameObject(context, "KeyController"),
		_unloaded(false)
	{ SetDepth(-1000); }
	void KeyController::Load() { Deactivate(); }
	void KeyController::Unload() { _unloaded = true; }

	void KeyController::AddDoor(ColoredDoor& door)
	{
		if (_unloaded) return;
		_doors.insert(std::make_pair(door.GetColor(), &door));
	}

	bool KeyController::RemoveDoor(ColoredDoor& door)
	{
		if (_unloaded) return false;

		// Find the door in the collection
		auto range = _doors.equal_range(door.GetColor());
		for (auto it = range.first; it != range.second; ++it)
		{
			if ((*it).second != &door) continue;
			// And remove it
			_doors.erase(it);
			return true;
		}
		return false;
	}

	void KeyController::AddKey(sf::Color color)
	{
		if (_unloaded) return;
		auto it = _keyCounts.find(color);
		if (it == _keyCounts.end())
		{
			_keyCounts.insert(std::make_pair(color, 1));
			return;
		}
		_keyCounts[color] = it->second + 1;
	}
	void KeyController::RemoveKey(sf::Color color)
	{
		if (_unloaded) return;
		auto it = _keyCounts.find(color);
		if (it == _keyCounts.end()) { return; }
		
		if (it->second != 1)
		{
			_keyCounts[color] = it->second - 1;
			return;
		}
		_keyCounts[color] = it->second - 1;

		// Open doors
		auto range = _doors.equal_range(color);
		for (auto it = range.first; it != range.second; ++it)
		{
			it->second->OnOpened();
		}
	}

	void KeyController::Update(const float deltaTime) { }
	void KeyController::Draw(sf::RenderTarget& target, sf::RenderStates& states) const { }
}