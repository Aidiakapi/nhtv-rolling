/*

Application's flow is fairly simple.
The entry point creates a window, clock and GameContext.
It also loads the first Scene, and calculates delta times.

Until any scene calls sf::Window::close() it will keep on,
processing everything. The destruction of GameContext should
handles the cleanup of the game.

*/

#include "stdafx.hpp"
#include "GameContext.hpp"
#include "SceneLevel1.hpp"

int main()
{
	using namespace std;
	using namespace ak;
	uptr<GameContext> context;
	{
		sf::ContextSettings ctxSets;
		ctxSets.antialiasingLevel = 4;
		auto windowPtr = make_unique<sf::RenderWindow>(
			// Initial config, scenes can always recreate the window
			sf::VideoMode(800, 600),
			"NHTV Rolling",
			sf::Style::Close | sf::Style::Titlebar | sf::Style::Resize,
			ctxSets);
		windowPtr->setFramerateLimit(60);
		context = make_unique<GameContext>(move(windowPtr));
	}

	sf::Clock clock;
	float deltaTime = 0.f;

	{
		// Setup the default keybinds, should probably have an interface for this.
		// But that's for another time.
		auto& input = context->GetInput();
		input.AddBinding(Input::MoveLeft, Binding(sf::Keyboard::Key::A));
		input.AddBinding(Input::MoveLeft, Binding(sf::Keyboard::Key::Left));
		input.AddBinding(Input::MoveRight, Binding(sf::Keyboard::Key::D));
		input.AddBinding(Input::MoveRight, Binding(sf::Keyboard::Key::Right));
		input.AddBinding(Input::Jump, Binding(sf::Keyboard::Key::W));
		input.AddBinding(Input::Jump, Binding(sf::Keyboard::Key::Up));
		input.AddBinding(Input::SpawnObject, Binding(sf::Keyboard::Key::Space));
		input.AddBinding(Input::SpawnObject, Binding(sf::Mouse::Button::Right));
		input.AddBinding(Input::Reset, Binding(sf::Keyboard::R));
		input.AddBinding(Input::Reset, Binding(sf::Keyboard::F10));
		input.AddBinding(Input::DebugDrawToggle, Binding(sf::Keyboard::F1));
		input.AddBinding(Input::DebugDrawClear, Binding(sf::Keyboard::F2));
		input.AddBinding(Input::DebugDrawShapes, Binding(sf::Keyboard::F3));
		input.AddBinding(Input::DebugDrawJoints, Binding(sf::Keyboard::F4));
		input.AddBinding(Input::DebugDrawAabb, Binding(sf::Keyboard::F5));
		input.AddBinding(Input::DebugDrawCenterOfMass, Binding(sf::Keyboard::F6));
	}

	// Startup scene
	context->LoadScene<SceneLevel1>();

	while (context->GetWindow().isOpen())
	{
		context->Update(deltaTime);
		context->Draw();
		deltaTime = clock.restart().asSeconds();
	}
}