#include "stdafx.hpp"
#include "GameContext.hpp"
#include "InstanceManager.hpp"
#include "GameObject.hpp"

namespace ak
{
	// Initialize the next id
	std::atomic<sf::Uint32> GameObject::_nextId = 0;

	GameObject::GameObject(GameContext& context, std::string name) :
		_id(_nextId.fetch_add(1)),
		_active(false),
		Visible(true),
		Persistent(false),
		_name(name),
		_depth(0),
		_context(context)
	{ }

	GameObject::~GameObject() { }

	bool GameObject::IsNamed() const { return !_name.empty(); }
	const std::string& GameObject::GetName() const { return _name; }

	void GameObject::Load() { }
	void GameObject::Unload() { }

	void GameObject::Activate() { _context.GetInstances().ActivateObject(*this); }
	void GameObject::Deactivate() { _context.GetInstances().DeactivateObject(*this); }
	void GameObject::SetDepth(sf::Int32 depth) { _context.GetInstances().SetObjectDepth(*this, depth); }

	void GameObject::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		Draw(target, states);
	}
}