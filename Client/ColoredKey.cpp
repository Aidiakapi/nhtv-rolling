#include "stdafx.hpp"
#include "ColoredKey.hpp"

#include "GameContext.hpp"
#include "InstanceManager.hpp"

#include "KeyController.hpp"

namespace ak
{
	ColoredKey::ColoredKey(GameContext& context, sf::Color color, std::string name) :
		Box2dObject(context, name),
		_color(color),
		_pickedUp(false)
	{
		SetDepth(-11);
	}

	sf::Color ColoredKey::GetColor() const { return _color; }

	void ColoredKey::Load()
	{
		Box2dObject::Load();

		auto keyController = _context.GetInstances().FindByName("KeyController");
		if (keyController.begin() == keyController.end())
			throw std::logic_error("Cannot instanciate colored doors without a key controller.");

		static_cast<KeyController&>(*keyController.begin()).AddKey(_color);
	}

	void ColoredKey::Unload()
	{
		Box2dObject::Unload();

		pickUp();
	}

	void ColoredKey::pickUp()
	{
		if (_pickedUp) return;
		_pickedUp = true;

		auto keyController = _context.GetInstances().FindByName("KeyController");

		// Key controller already removed
		if (keyController.begin() == keyController.end()) return;

		static_cast<KeyController&>(*keyController.begin()).RemoveKey(_color);
	}
}