#pragma once

#define PI 3.14159265359f
#define PIOVER2 (PI/2)
#define PIOVER4 (PI/4)
#define PITIMES2 (PI*2)
#define DEGTORAD(x) ((x) * PI/180)
#define RADTODEG(x) ((x) * 180/PI)