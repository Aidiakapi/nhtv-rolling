/*

The game context defines the context in which the game is currently running.
Some examples of what it includes:
- Instance manager
- Resource manager
- Scene information
- Window handle
It is the owner of all this information.

Owned by the main method.

*/

#pragma once
#include "InstanceManager.hpp"
#include "ResourceManager.hpp"
#include "InputManager.hpp"
#include "Scene.hpp"

int main();
namespace ak
{
	class GameContext :
		public sf::NonCopyable
	{
		friend int ::main();
	public:
		GameContext(uptr<sf::RenderWindow> wnd);

		InstanceManager& GetInstances() const;
		ResourceManager& GetResources() const;
		InputManager& GetInput() const;

		sf::RenderWindow& GetWindow() const;

		inline Scene& GetScene() const { return *_scene; }

		void Update(const float deltaTime);
		void Draw() const;

		template<typename T, typename ...Args>
		inline void LoadScene(Args&& ...arguments)
		{
			uptr<T> scene = std::make_unique<T>(*this, std::forward<Args>(arguments)...);
			if (_isUpdating) _nextScene = std::move(scene);
			else loadScene(std::move(scene));
		}
	private:
		const uptr<sf::RenderWindow> _wnd;

		const uptr<InstanceManager> _instances;
		const uptr<ResourceManager> _resources;
		const uptr<InputManager> _input;

		void loadScene(uptr<Scene> scene);

		uptr<Scene> _scene;
		uptr<Scene> _nextScene;
		bool _isUpdating;
	};
}