/*

A collection of GameObjects that can be populated by a Scene,
other GameObjects and GameComponents.

Owns all GameObjects and will destroy them.

Owned by GameContext.

*/

#pragma once
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <queue>
#include <stack>
#include "GameObject.hpp"

namespace ak
{
	class GameContext;
	class InstanceManager :
		public sf::Drawable
	{
	public:
		class NamedGameObjects
		{
			friend InstanceManager;
		private:
			typedef std::unordered_set<GameObject*> Collection;

			class Iter
				: public std::iterator<Collection::const_iterator::iterator_category, GameObject&, Collection::const_iterator::difference_type, GameObject*, GameObject&>
			{
			public:
				Iter(const Collection::const_iterator baseIter);

				bool operator!= (const Iter& other) const { return _baseIter != other._baseIter; }
				bool operator== (const Iter& other) const { return _baseIter == other._baseIter; }
				GameObject& operator* () const { return **_baseIter; }
				const Iter& operator++ ()
				{
					++_baseIter;
					return *this;
				}
			private:
				Collection::const_iterator _baseIter;
			};

			NamedGameObjects(const Collection& data);
			const Collection& _data;

			static NamedGameObjects Empty;
		public:
			typedef Iter iterator;
			iterator begin() const;
			iterator end() const;
		};

		InstanceManager(GameContext& context);

		// Creates GameObject and instanciates it 
		template <typename T, typename ...Args>
		inline T& Create(Args&& ...arguments)
		{
			return CreateAt<T>(0, 0, 0, std::forward<Args>(arguments)...);
		}

		template <typename T, typename ...Args>
		inline T& CreateAt(float x, float y, float angle, Args&& ...arguments)
		{
			uptr<T> ptr = std::make_unique<T>(_context, std::forward<Args>(arguments)...);
			ptr->setPosition(x, y);
			ptr->setRotation(angle);
			T* rawPtr = ptr.get();
			if (_isUpdating)
			{
				static_cast<GameObject*>(rawPtr)->_active = false;
				_initQueue.push(std::move(ptr));
			}
			else createGameObject(std::move(ptr));
			return *rawPtr;
		}

		void ActivateObject(GameObject& obj);
		void DeactivateObject(GameObject& obj);

		void SetObjectDepth(GameObject& obj, sf::Int32 depth);
		
		void Remove(GameObject& obj);
		// nullptr is ignored to support Remove(operator[](someObjectId)) multiple times
		void Remove(GameObject* obj);
		void Clean();

		void Update(const float deltaTime);

		const NamedGameObjects FindByName(const std::string& name) const;
		
		// Gets objects by their ID
		GameObject* const operator[](const sf::Uint32 id) const;
	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		void createGameObject(uptr<GameObject> gobject);

		// Contains all GameObjects
		std::unordered_map<sf::Uint32, uptr<GameObject>> _gameObjects;

		// Maps names to game objects
		std::unordered_map<std::string, NamedGameObjects::Collection> _nameMap;

		// Update/draw order map
		std::multimap<sf::Int32, GameObject*, std::greater<sf::Int32>> _depthMap;

		std::queue<uptr<GameObject>> _initQueue;
		std::stack<sf::Uint32> _removeStack;

		// Reference will remain valid because this object lives for as long
		// as the GameContext lives.
		GameContext& _context;

		bool _isUpdating;
	};
}