#pragma once
#include "Box2dObject.hpp"

namespace ak
{
	class Player :
		public Box2dObject
	{
	public:
		Player(GameContext& context);
		virtual ~Player();

		b2Body* CreateBody(b2World& world, ContactManager& contactManager) override;

		void Update(const float deltaTime) override;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override;

		void Unload() override;
	};
}