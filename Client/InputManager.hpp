#pragma once
#include <vector>
#include <unordered_map>
#include "Input.hpp"
#include "Binding.hpp"

namespace ak
{
	class Scene;
	class InputManager final :
		sf::NonCopyable
	{
	private:
		typedef std::unordered_multimap<Input::InputType, const Binding> BindingMap;
	public:
		typedef BindingMap::const_iterator const_iterator;
		typedef std::vector<const std::pair<const Input::InputType, const Binding>> BindingList;

		InputManager(sf::Window& inputWindow);

		// Binding
		
		// Iterate bindings
		inline const const_iterator begin() const { return _bindings.begin(); }
		inline const const_iterator end() const { return _bindings.end(); }
		
		// Iterate specific bindings
		inline const std::pair<const const_iterator, const const_iterator> GetBindings(Input::InputType input) const
		{ return _bindings.equal_range(input); }
		
		const BindingList GetBindings(const Binding binding) const;

		// Modify 
		void AddBinding(Input::InputType input, const Binding binding);
		inline void ClearBindings() { _bindings.clear(); }
		void ClearBindings(Input::InputType input);

		// Update input
		void Update(Scene& scene);

		// Getting input
		inline const sf::Uint32 GetTextEntered() const { return _textEntered; }

		const bool IsKeyDown(const sf::Keyboard::Key key) const;
		const bool IsKeyPressed(const sf::Keyboard::Key key) const;
		const bool IsKeyReleased(const sf::Keyboard::Key key) const;

		inline const bool IsMouseDown(const sf::Mouse::Button button) const { return _mouseButtonDown[button]; }
		inline const bool IsMousePressed(const sf::Mouse::Button button) const { return _mouseButtonPressed[button]; }
		inline const bool IsMouseReleased(const sf::Mouse::Button button) const { return _mouseButtonReleased[button]; }

		inline const bool IsDown(const Input::InputType input) const
		{ return isBindingAnything(&InputManager::IsKeyDown, &InputManager::IsMouseDown, input); }
		inline const bool IsPressed(const Input::InputType input) const
		{ return isBindingAnything(&InputManager::IsKeyPressed, &InputManager::IsMousePressed, input); }
		inline const bool IsReleased(const Input::InputType input) const
		{ return !IsDown(input) && isBindingAnything(&InputManager::IsKeyReleased, &InputManager::IsMouseReleased, input); }

	private:

		template<typename TKeyAction, typename TMouseAction>
		const bool isBindingAnything(TKeyAction keyAction, TMouseAction mouseAction, const Input::InputType input) const
		{
			auto range = _bindings.equal_range(input);
			for (auto it = range.first; it != range.second; ++it)
			{
				if (it->second.Type == Binding::BindingType::Key)
				{
					if ((*this.*keyAction)(static_cast<sf::Keyboard::Key>(it->second.Value))) return true;
				}
				else
				{
					if ((*this.*mouseAction)(static_cast<sf::Mouse::Button>(it->second.Value))) return true;
				}
			}
			return false;
		}

		sf::Window& _wnd;

		// Specify all bindings to a specific input
		BindingMap _bindings;

		// Holds a collection of all pressed keys
		std::vector<const sf::Keyboard::Key> _keyDown;
		// Keys that were just pressed
		std::vector<const sf::Keyboard::Key> _keyPressed;
		// And keys that have been released
		std::vector<const sf::Keyboard::Key> _keyReleased;
		// It'd be O(1) lookups etc if we'd use an unordered_set.
		// However the chances of having so many keys pressed that
		// it actually becomes faster than a plain vector are slim.
		// In real scenarios having the amount of buttons pressed
		// that'd yield performance benefits is rare.

		// Same goes for the mouse buttons
		const uptr<bool[]> _mouseButtonDown;
		const uptr<bool[]> _mouseButtonPressed;
		const uptr<bool[]> _mouseButtonReleased;
		// Because there are only 5 mouse buttons (at the time) we
		// store the state for all of em.

		bool _isMouseButtonPressed;
		bool _isMouseButtonReleased;

		sf::Uint32 _textEntered;
	};
}