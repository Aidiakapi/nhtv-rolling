#include "stdafx.hpp"
#include "SimpleKey.hpp"

#include "GameContext.hpp"
#include "ResourceManager.hpp"
#include "ContactManager.hpp"
#include "CollisionCategory.hpp"

namespace ak
{
	SimpleKey::SimpleKey(GameContext& context, sf::Color color) :
		ColoredKey(context, color),
		_hasContact(false)
	{
		SetDepth(-5);
	}

	b2Body* SimpleKey::CreateBody(b2World& world, ContactManager& contactManager)
	{
		b2BodyDef bDef;
		bDef.type = b2_staticBody;
		bDef.position = b2Vec2(getPosition().x, getPosition().y);
		bDef.angle = DEGTORAD(getRotation());

		b2PolygonShape shp;
		shp.SetAsBox(0.5f, 0.75f);
		b2FixtureDef fDef;
		fDef.shape = &shp;
		fDef.filter.categoryBits = CollisionCategory::Entity;
		fDef.filter.maskBits = CollisionCategory::Player;
		fDef.isSensor = true;
		
		auto body = world.CreateBody(&bDef);
		auto fix = body->CreateFixture(&fDef);

		_beginContact = contactManager.RegisterBeginContact(fix, std::bind(&SimpleKey::handleBeginContact, this, std::placeholders::_1));

		return body;
	}

	void SimpleKey::Load()
	{
		ColoredKey::Load();
		auto sprite = std::make_unique<Box2dSprite>(_context.GetResources().GetTexture("Key.png"));
		sprite->setOrigin(15.f, 22.5f);
		sprite->setColor(GetColor());

		_sprite = std::move(sprite);
	}

	void SimpleKey::Update(const float deltaTime)
	{
		if (_hasContact)
		{
			pickUp();
			_context.GetInstances().Remove(*this);
		}
	}
	
	void SimpleKey::Draw(sf::RenderTarget& target, sf::RenderStates& states) const
	{
		target.draw(*_sprite, states);
	}

	void SimpleKey::Unload()
	{
		auto fix = _body->GetFixtureList();
		auto& cmgr = getContactManager();
		cmgr.UnregisterBeginContact(fix, _beginContact);

		ColoredKey::Unload();
	}

	void SimpleKey::handleBeginContact(b2Contact* contact)
	{
		_hasContact = true;
	}
}