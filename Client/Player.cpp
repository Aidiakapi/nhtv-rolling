#include "stdafx.hpp"
#include "Player.hpp"

#include "CollisionCategory.hpp"
#include "GameContext.hpp"
#include "InputManager.hpp"
#include "ContactManager.hpp"

#include "SceneLevel.hpp"

#define PLAYER_RADIUS 0.5f
#define JUMP_DIST 0.3f
#define JUMP_CASTS_PER_SIDE 2
#define JUMP_CAST_ANGLE_OFFSET (PIOVER4 / 4.f)

namespace ak
{
	Player::Player(GameContext& context) :
		Box2dObject(context, "Player")
	{
		SetDepth(-10);
	}
	Player::~Player() { }

	b2Body* Player::CreateBody(b2World& world, ContactManager& contactManager)
	{
		// The main body (circle)
		b2BodyDef bDef;
		bDef.type = b2_dynamicBody;
		bDef.angularDamping = 0.5f;
		auto& pos = getPosition();
		bDef.position.Set(pos.x, pos.y);
		bDef.angle = DEGTORAD(getRotation());
		bDef.bullet = true;

		b2Body* player = world.CreateBody(&bDef);

		b2CircleShape shp;
		shp.m_radius = PLAYER_RADIUS;

		b2FixtureDef fDef;
		fDef.friction = 0.6f;
		fDef.shape = &shp;
		fDef.density = 1.f;
		fDef.restitution = 0.2f;
		fDef.filter.categoryBits = CollisionCategory::Player;

		player->CreateFixture(&fDef);
		return player;
	}

	namespace
	{
		// Jump detection raycast class
		class JumpRayCast : public b2RayCastCallback
		{
		public:
			JumpRayCast() :
				DidCollide(false)
			{ }

			float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point,
				const b2Vec2& normal, float32 fraction)
			{
				// If the fraction is larger than 1.f, it means it's out of range of the ray.
				if (fraction > 1.f) return 0.f;

				auto cat = fixture->GetFilterData().categoryBits;
				if (cat && CollisionCategory::Environment
					|| cat && CollisionCategory::EnvironmentProp)
				{
					// If the fixture in the ray is a part of the environment
					// set the flag and stop the raycast early.
					DidCollide = true;
					return 0;
				}
				// Otherwise filter it (ie. treat it as if it doesn't exist)
				return -1.f;
			}

			bool DidCollide;
		};
	}

	void Player::Update(const float deltaTime)
	{
		auto& input = _context.GetInput();
		auto left = input.IsDown(Input::MoveLeft);
		auto right = input.IsDown(Input::MoveRight);

		// Do nothing if left and right are active, or neither left nor right are active.
		if (left != right)
		{
			float mult = left ? -1.f : 1.f;

			auto hspeed = _body->GetLinearVelocity().x;
			auto absHspeed = abs(hspeed);
			// If the player is moving rapid in the opposite direction
			// that the player wants to go...
			if (absHspeed > 2.f && std::signbit(hspeed) == right)
			{
				// ...increase the breaking force
				mult *= std::min(6.f, absHspeed / 2.f);
			}

			_body->ApplyForceToCenter(b2Vec2(6.f * mult, 0.f), true);
			_body->ApplyTorque(mult * -4.f, true);
		}

		if (input.IsPressed(Input::Jump))
		{
			// Cast rays to determine if you can jump
			auto& world = getWorld();
			JumpRayCast rayCastCallback;
			auto center = _body->GetPosition();
			for (int i = -JUMP_CASTS_PER_SIDE; !rayCastCallback.DidCollide && i <= JUMP_CASTS_PER_SIDE; ++i)
			{
				auto angle = (PI + PIOVER2) + (i * JUMP_CAST_ANGLE_OFFSET);
				world.RayCast(&rayCastCallback,
					center,
					center + b2Vec2(
					cos(angle) * (JUMP_DIST + PLAYER_RADIUS),
					sin(angle) * (JUMP_DIST + PLAYER_RADIUS)));
			}

			if (rayCastCallback.DidCollide)
			{
				auto& vel = _body->GetLinearVelocity();
				auto y = vel.y;
				if (y < 0.f) y /= 2.f;
				_body->SetLinearVelocity(b2Vec2(vel.x, std::min(3.f, std::max(-2.f, y))));
				_body->ApplyLinearImpulse(b2Vec2(0.f, 8.f), _body->GetWorldCenter(), true);
			}
		}

		if (input.IsPressed(Input::Reset))
		{
			static_cast<SceneLevel&>(_context.GetScene()).OnPlayerDied();
			return;
		}

		Box2dObject::Update(deltaTime);
	}

	void Player::Draw(sf::RenderTarget& target, sf::RenderStates& states) const
	{
		sf::CircleShape circle(PLAYER_RADIUS);
		circle.setFillColor(sf::Color::Red);
		circle.setOrigin(PLAYER_RADIUS, PLAYER_RADIUS);
		sf::RectangleShape rect(sf::Vector2f(PLAYER_RADIUS, 0.02f));
		rect.setFillColor(sf::Color::Blue);
		rect.setOrigin(0.f, 0.01f);
		
		target.draw(circle, states);
		target.draw(rect, states);
	}

	void Player::Unload()
	{
		// Destroys _body
		Box2dObject::Unload();
	}
}

#undef PLAYER_RADIUS
#undef JUMP_DIST
#undef JUMP_CASTS_PER_SIDE
#undef JUMP_CAST_ANGLE_OFFSET