#pragma once
#include "Box2dObject.hpp"

namespace ak
{
	class DeathZone :
		public Box2dObject
	{
	public:
		DeathZone(GameContext& context, const sf::Vector2f size);

		b2Body* CreateBody(b2World& world, ContactManager& contactManager) override;

		void Update(const float deltaTime) override;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override;
		void Unload() override;

	private:
		void setFlag(b2Contact* contact);

		const sf::Vector2f _size;
		sf::Uint32 _beginContactListener;
		bool _madeContact;
	};
}