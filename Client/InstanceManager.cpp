#include "stdafx.hpp"
#include <vector>
#include "InstanceManager.hpp"
#include "GameObject.hpp"

namespace ak
{
	InstanceManager::InstanceManager(GameContext& context) :
		// Initial capacity
		_gameObjects(16),
		_context(context),
		_isUpdating(false)
	{
		
	}
	
	// Creation
	void InstanceManager::createGameObject(uptr<GameObject> object)
	{
		GameObject* obj = object.get();
		_gameObjects.insert(std::make_pair(obj->GetID(), std::move(object)));
		_depthMap.insert(std::make_pair(obj->_depth, obj));
		obj->_active = true;

		obj->Load();
		if (obj->IsNamed())
		{
			// Add to named collection
			auto find = _nameMap.find(obj->GetName());
			if (find == _nameMap.end())
			{
				// If there's no object with this name yet we create a new collection to hold it
				_nameMap.insert(std::make_pair(obj->GetName(), InstanceManager::NamedGameObjects::Collection({ obj })));
			}
			// Otherwise just insert the entry
			else find->second.insert(obj);
		}
	}

	// Removal
	void InstanceManager::Remove(GameObject& object)
	{
		auto find = _gameObjects.find(object.GetID());
		if (find == _gameObjects.end()
			|| find->second.get() != &object)
			throw std::invalid_argument("Argument 'object' is not owned by this InstanceManager.");

		if (_isUpdating)
		{
			_removeStack.push(object.GetID());
			return;
		}

		if (object._active) DeactivateObject(object);
		object.Unload();

		uptr<GameObject> objPtr = std::move(find->second);
		_gameObjects.erase(find);

		// Remove from name collection
		if (object.IsNamed())
		{
			auto find = _nameMap.find(object.GetName());
			if (find != _nameMap.end())
			{
				auto find2 = find->second.find(&object);
				if (find2 != find->second.end())
					find->second.erase(find2);
			}
		}
	}
	void InstanceManager::Remove(GameObject* object)
	{
		if (object == nullptr) return;
		Remove(*object);
	}

	void InstanceManager::Clean()
	{
		std::vector<GameObject*> removeList;
		removeList.reserve(_gameObjects.size());
		for (auto& pair : _gameObjects)
		{
			if (pair.second == nullptr || pair.second->Persistent) continue;
			removeList.push_back(pair.second.get());
		}

		struct GameObjectDepthSorter
		{
			bool operator() (GameObject* i, GameObject* j)
			{ return (i->GetDepth() > j->GetDepth()); }
		} sorter;
		std::sort(removeList.begin(), removeList.end(), sorter);

		for (GameObject* const ptr : removeList) Remove(ptr);
	}

	// State updates (active/depth)
	void InstanceManager::ActivateObject(GameObject& obj)
	{
		if (obj._active) return;
		obj._active = true;
		_depthMap.insert(std::make_pair(obj._depth, &obj));
	}

	void InstanceManager::DeactivateObject(GameObject& obj)
	{
		if (!obj._active) return;
		obj._active = false;
		// Remove from depth map
		auto range = _depthMap.equal_range(obj._depth);
		for (auto it = range.first; it != range.second; it++)
		{
			if (it->second->_id != obj._id) continue;
			_depthMap.erase(it);
			break;
		}
	}

	void InstanceManager::SetObjectDepth(GameObject& obj, sf::Int32 depth)
	{
		if (!obj._active)
		{
			obj._depth = depth;
			return;
		}
		// Remove from old position
		auto range = _depthMap.equal_range(obj._depth);
		for (auto it = range.first; it != range.second; it++)
		{
			if (it->second->_id != obj._id) continue;
			_depthMap.erase(it);
			break;
		}
		// Insert new position
		obj._depth = depth;
		_depthMap.insert(std::make_pair(depth, &obj));
	}

	// Updating and drawing
	void InstanceManager::Update(const float deltaTime)
	{
		_isUpdating = true;
		for (const std::pair<const sf::Int32, GameObject*>& pair : _depthMap)
		{
			pair.second->Update(deltaTime);
		}
		_isUpdating = false;
		while (!_removeStack.empty())
		{
			auto top = _removeStack.top();
			_removeStack.pop();
			Remove((*this)[top]);
		}

		while (!_initQueue.empty())
		{
			uptr<GameObject> gob = std::move(_initQueue.front());
			_initQueue.pop();
			createGameObject(std::move(gob));
		}
	}

	void InstanceManager::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		for (const std::pair<const sf::Int32, GameObject*>& pair : _depthMap)
		{
			if (pair.second->Visible)
				target.draw(*pair.second, states);
		}
	}

	// Lookup by ID
	GameObject* const InstanceManager::operator[](const sf::Uint32 id) const
	{
		auto res = _gameObjects.find(id);
		return res == _gameObjects.end() ? nullptr : res->second.get();
	}

	// Iterating by name
	const InstanceManager::NamedGameObjects InstanceManager::FindByName(const std::string& name) const
	{
		auto find = _nameMap.find(name);
		if (find == _nameMap.end()) return NamedGameObjects::Empty;
		return NamedGameObjects(find->second);
	}

	InstanceManager::NamedGameObjects::NamedGameObjects(const Collection& data) :
		_data(data)
	{ }
	InstanceManager::NamedGameObjects::iterator InstanceManager::NamedGameObjects::begin() const
	{ return _data.begin(); }
	InstanceManager::NamedGameObjects::iterator InstanceManager::NamedGameObjects::end() const
	{ return _data.end(); }

	// The iterator wrapper
	InstanceManager::NamedGameObjects::Iter::Iter(const Collection::const_iterator baseIter) :
		_baseIter(baseIter)
	{ }

	// One empty collection
	InstanceManager::NamedGameObjects InstanceManager::NamedGameObjects::Empty(*(new InstanceManager::NamedGameObjects::Collection()));
}