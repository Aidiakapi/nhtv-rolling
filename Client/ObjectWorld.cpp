#include "stdafx.hpp"
#include "ObjectWorld.hpp"

namespace ak
{
	ObjectWorld::ObjectWorld(GameContext& context) :
		GameObject(context)
	{
		Visible = false;
		SetDepth(-80000);
	}

	void ObjectWorld::Load()
	{
		_world = std::make_unique<b2World>(b2Vec2(0.f, -12.f));
		_contactManager = std::make_unique<ContactManager>();
		_world->SetContactListener(_contactManager.get());
	}

	void ObjectWorld::Update(const float deltaTime)
	{
		_world->Step(deltaTime, 8, 4);
	}
}