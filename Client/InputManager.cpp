#include "stdafx.hpp"
#include "InputManager.hpp"
#include "Scene.hpp"
#include "Binding.hpp"

namespace ak
{
	InputManager::InputManager(sf::Window& wnd) :
		_wnd(wnd),

		_mouseButtonDown(std::make_unique<bool[]>(sf::Mouse::Button::ButtonCount)),
		_mouseButtonPressed(std::make_unique<bool[]>(sf::Mouse::Button::ButtonCount)),
		_mouseButtonReleased(std::make_unique<bool[]>(sf::Mouse::Button::ButtonCount)),

		_isMouseButtonPressed(false),
		_isMouseButtonReleased(false),

		_textEntered(0)
	{
		for (int i = 0; i < sf::Mouse::Button::ButtonCount; i++)
		{
			_mouseButtonDown[i] = false;
			_mouseButtonPressed[i] = false;
			_mouseButtonReleased[i] = false;
		}
	}

	// Process input events
	void InputManager::Update(Scene& scene)
	{
		if (_isMouseButtonPressed)
		{
			for (int i = 0; i < sf::Mouse::Button::ButtonCount; ++i)
				_mouseButtonPressed[i] = false;
			_isMouseButtonPressed = false;
		}
		if (_isMouseButtonReleased)
		{
			for (int i = 0; i < sf::Mouse::Button::ButtonCount; ++i)
				_mouseButtonReleased[i] = false;
			_isMouseButtonReleased = false;
		}

		_keyPressed.clear();
		_keyReleased.clear();

		_textEntered = 0;

		bool focusLost = false;

		sf::Event event;
		while (_wnd.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::EventType::MouseButtonPressed:
				_mouseButtonDown[event.mouseButton.button] = true;
				_mouseButtonPressed[event.mouseButton.button] = true;
				_isMouseButtonPressed = true;
				break;
			case sf::Event::EventType::MouseButtonReleased:
				_mouseButtonDown[event.mouseButton.button] = false;
				_mouseButtonReleased[event.mouseButton.button] = true;
				_isMouseButtonReleased = true;
				break;

			case sf::Event::EventType::KeyPressed:
				if (IsKeyDown(event.key.code)) break;
				_keyDown.push_back(event.key.code);
				_keyPressed.push_back(event.key.code);
				break;
			case sf::Event::EventType::KeyReleased:
			{
				std::vector<sf::Keyboard::Key>::iterator it = _keyDown.begin();
				for (; it != _keyDown.end(); ++it)
				{
					if (*it == event.key.code)
					{
						_keyDown.erase(it);
						_keyReleased.push_back(event.key.code);
						break;
					}
				}
				break;
			}

			case sf::Event::EventType::TextEntered:
				_textEntered = event.text.unicode;
				break;

			case sf::Event::EventType::LostFocus:
				focusLost = true;
			default:
				scene.HandleEvent(event);
				break;
			}
		}
		if (!focusLost) return;
		
		// Reset the input when focus is lost
		_keyDown.clear();
		_keyPressed.clear();
		_keyReleased.clear();
		for (int i = 0; i < sf::Mouse::Button::ButtonCount; i++)
		{
			_mouseButtonDown[i] = false;
			_mouseButtonPressed[i] = false;
			_mouseButtonReleased[i] = false;
			_isMouseButtonPressed = false;
			_isMouseButtonReleased = false;
		}
	}

	const bool InputManager::IsKeyDown(const sf::Keyboard::Key key) const
	{
		for (sf::Keyboard::Key k : _keyDown)
		{
			if (k == key) return true;
		}
		return false;
	}
	const bool InputManager::IsKeyPressed(const sf::Keyboard::Key key) const
	{
		for (sf::Keyboard::Key k : _keyPressed)
		{
			if (k == key) return true;
		}
		return false;
	}
	const bool InputManager::IsKeyReleased(const sf::Keyboard::Key key) const
	{
		for (sf::Keyboard::Key k : _keyReleased)
		{
			if (k == key) return true;
		}
		return false;
	}

	// Binding management
	const InputManager::BindingList InputManager::GetBindings(const Binding binding) const
	{
		BindingList list;
		for (auto pair : _bindings)
		{
			if (pair.second == binding) list.push_back(pair);
		}
		return std::move(list);
	}

	void InputManager::AddBinding(Input::InputType input, const Binding binding)
	{
		// Get the range of inputs for this binding
		auto range = _bindings.equal_range(input);
		for (auto it = range.first; it != range.second; ++it)
		{
			// A pair of input, binding already exists
			if (it->second == binding) return;
		}
		_bindings.insert(std::make_pair(input, binding));
	}

	void InputManager::ClearBindings(Input::InputType input)
	{
		auto range = _bindings.equal_range(input);
		_bindings.erase(range.first, range.second);
	}
}