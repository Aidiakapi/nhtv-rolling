#include "stdafx.hpp"
#include "ColoredDoor.hpp"

#include "GameContext.hpp"
#include "InstanceManager.hpp"

#include "KeyController.hpp"

namespace ak
{
	ColoredDoor::ColoredDoor(GameContext& context, sf::Color color, std::string name) :
		Box2dObject(context, name),
		_color(color)
	{ }

	sf::Color ColoredDoor::GetColor() const { return _color; }

	void ColoredDoor::Load()
	{
		Box2dObject::Load();

		auto keyController = _context.GetInstances().FindByName("KeyController");
		if (keyController.begin() == keyController.end())
			throw std::logic_error("Cannot instanciate colored doors without a key controller.");

		static_cast<KeyController&>(*keyController.begin()).AddDoor(*this);
	}

	void ColoredDoor::Unload()
	{
		Box2dObject::Unload();

		auto keyController = _context.GetInstances().FindByName("KeyController");

		// Key controller already removed
		if (keyController.begin() == keyController.end()) return;

		static_cast<KeyController&>(*keyController.begin()).RemoveDoor(*this);
	}
}