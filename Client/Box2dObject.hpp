#pragma once
#include "GameObject.hpp"

namespace ak
{
	class ContactManager;
	class Box2dObject :
		public GameObject
	{
	public:
		Box2dObject(GameContext& context, std::string name = "");

		virtual b2Body* CreateBody(b2World& world, ContactManager& contactManager) = 0;

		void Load() override;
		void Unload() override;
		void Update(const float deltaTime) override = 0;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override = 0;
	protected:
		b2Body* _body;

		b2World& getWorld();
		const b2World& getWorld() const;
		ContactManager& getContactManager();

		// sf::Drawable
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override final;
	};
}