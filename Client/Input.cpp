// Auto-generated. Changes will be overwritten when compiled.
#include "stdafx.hpp"
#include "Input.hpp"
#include <sstream>

namespace ak
{
    const Input::InputType Input::GetType(const std::string& name)
    {
        auto it = _valueMap.find(name);
        return it == _valueMap.end()
            ? static_cast<InputType>(0)
            : it->second;
    }

    const std::string& Input::GetName(const Input::InputType type)
    {
        if (type >= 1 && type <= 4) return _names[type - 1];
        std::stringstream str;
        str << "Invalid Input::InputType specified. Got: " << static_cast<sf::Uint16>(type) << ".";
        throw std::invalid_argument(str.str());
    }

    const sf::Uint16 Input::Count = 11;
    const Input Input::Items = Input();
    const std::string* Input::begin() const { return _names; }
    const std::string* Input::end() const { return _names + Count; }

    const std::unordered_map<std::string, const Input::InputType> Input::_valueMap = ([]()
    {
        std::unordered_map<std::string, const Input::InputType> valueMap;
        valueMap.insert(std::make_pair("MoveLeft", static_cast<Input::InputType>(1)));
        valueMap.insert(std::make_pair("MoveRight", static_cast<Input::InputType>(2)));
        valueMap.insert(std::make_pair("Jump", static_cast<Input::InputType>(3)));
        valueMap.insert(std::make_pair("SpawnObject", static_cast<Input::InputType>(4)));
        valueMap.insert(std::make_pair("Reset", static_cast<Input::InputType>(5)));
        valueMap.insert(std::make_pair("DebugDrawToggle", static_cast<Input::InputType>(6)));
        valueMap.insert(std::make_pair("DebugDrawClear", static_cast<Input::InputType>(7)));
        valueMap.insert(std::make_pair("DebugDrawShapes", static_cast<Input::InputType>(8)));
        valueMap.insert(std::make_pair("DebugDrawJoints", static_cast<Input::InputType>(9)));
        valueMap.insert(std::make_pair("DebugDrawAabb", static_cast<Input::InputType>(10)));
        valueMap.insert(std::make_pair("DebugDrawCenterOfMass", static_cast<Input::InputType>(11)));
        return valueMap;
    })();

    const std::string* const Input::_names = ([]()
    {
        auto strArr = new std::string[11];
        strArr[0] = "MoveLeft";
        strArr[1] = "MoveRight";
        strArr[2] = "Jump";
        strArr[3] = "SpawnObject";
        strArr[4] = "Reset";
        strArr[5] = "DebugDrawToggle";
        strArr[6] = "DebugDrawClear";
        strArr[7] = "DebugDrawShapes";
        strArr[8] = "DebugDrawJoints";
        strArr[9] = "DebugDrawAabb";
        strArr[10] = "DebugDrawCenterOfMass";
        return strArr;
    })();
}
