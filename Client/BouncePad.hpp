#pragma once
#include "Box2dObject.hpp"

namespace ak
{
	class BouncePad :
		public Box2dObject
	{
	public:
		BouncePad(GameContext& context, float width, float height, float distance, float force, float speed);
		~BouncePad();

		b2Body* CreateBody(b2World& world, ContactManager& contactManager) override;

		void Update(const float deltaTime) override;
		void Draw(sf::RenderTarget& target, sf::RenderStates& states) const override;
		void Unload() override;

	private:
		b2Body* _baseBody;
		const float _width;
		const float _height;
		const float _dist;
		const float _force;
		const float _speed;
	};
}